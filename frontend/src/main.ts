import './assets/main.css'
import "@mdi/font/css/materialdesignicons.css"

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { getInterceptorFunctions } from '@/repositories/ApiInterceptors'
import colors from 'vuetify/lib/util/colors'
import axios from 'axios'

const pinia_instance = createPinia()

const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'dark',
    themes: {
      light: {
        dark: false,
        colors: {
          background: '#eee',
          something: colors.blue.lighten4
        }
      },
      dark: {
        dark: true,
        colors: {
          something: colors.blue.darken4
        }
      }
    }
  }
})

const app = createApp(App)

app.use(pinia_instance)
app.use(router)
app.use(vuetify)
app.mount('#app')

const {
  responseUnauthorizedRedirect,
  responseValidVoidInterceptor,
  requestAuthenticate,
} = getInterceptorFunctions()

axios.interceptors.response.use(responseValidVoidInterceptor, responseUnauthorizedRedirect)
axios.interceptors.request.use(requestAuthenticate)
