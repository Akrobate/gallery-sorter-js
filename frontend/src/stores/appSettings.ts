import { ref, watch } from 'vue'
import { defineStore } from 'pinia'

export const useAppSettings = defineStore('appSettings', () => {

    const LOCAL_STORAGE_ITEM_NAME = 'gallery_sorter_settings'

    const settings = ref<any>({})
    const show_settings = ref(false)

    function resetSettings() {
        localStorage.removeItem(LOCAL_STORAGE_ITEM_NAME)
        settings.value = {}
    }

    function setSettingsToLocalSotrage() {
        const _settings_string = JSON.stringify(settings.value)
        localStorage.setItem(LOCAL_STORAGE_ITEM_NAME, _settings_string)
    }

    function getSettingsFromLocalStorage() {
        const _settings = localStorage.getItem(LOCAL_STORAGE_ITEM_NAME)
        if (_settings) {
            settings.value = JSON.parse(_settings)
        }
        return settings.value
    }

    function reset() {
        resetSettings()
        setting_dark_mode.value = getInitialSettingDarkMode()
        setting_show_preview_image_in_gallery_list.value = getInitialSettingShowPreviewImageInGalleryList()
        setting_show_background_in_gallery_list.value = getInitialSettingShowBackgroundInGalleryList()
        setting_hightlight_never_played.value = getInitialSettingHightlightNeverPlayed()
        setting_activate_authenticatoion_token.value = getInitialSettingActivateAuthenticationToken()
        setting_authenticatoion_token.value = getInitialSettingAuthenticationToken()
        setting_display_archived.value = getInitialSettingDisplayArchived()
        setting_display_card_element_v2.value = getInitialSettingDisplayCardElementV2()
        setting_random_elo_rule.value = getInitialRandomEloRule()
    }


    const setting_dark_mode = ref(getInitialSettingDarkMode())

    function getInitialSettingDarkMode() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_dark_mode === undefined) {
            return true
        }
        return setting_object.setting_dark_mode
    }

    watch(setting_dark_mode, () => {
        settings.value.setting_dark_mode = setting_dark_mode.value
        setSettingsToLocalSotrage()
    })


    const setting_show_preview_image_in_gallery_list = ref(getInitialSettingShowPreviewImageInGalleryList())

    function getInitialSettingShowPreviewImageInGalleryList() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_show_preview_image_in_gallery_list === undefined) {
            return true
        }
        return setting_object.setting_show_preview_image_in_gallery_list
    }

    watch(setting_show_preview_image_in_gallery_list, () => {
        settings.value.setting_show_preview_image_in_gallery_list = setting_show_preview_image_in_gallery_list.value
        setSettingsToLocalSotrage()
    })


    const setting_show_background_in_gallery_list = ref(getInitialSettingShowBackgroundInGalleryList())

    function getInitialSettingShowBackgroundInGalleryList() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_show_background_in_gallery_list === undefined) {
            return false
        }
        return setting_object.setting_show_background_in_gallery_list
    }

    watch(setting_show_background_in_gallery_list, () => {
        settings.value.setting_show_background_in_gallery_list = setting_show_background_in_gallery_list.value
        setSettingsToLocalSotrage()
    })


    const setting_hightlight_never_played = ref(getInitialSettingHightlightNeverPlayed())

    function getInitialSettingHightlightNeverPlayed() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_hightlight_never_played === undefined) {
            return true
        }
        return setting_object.setting_hightlight_never_played
    }

    watch(setting_hightlight_never_played, () => {
        settings.value.setting_hightlight_never_played = setting_hightlight_never_played.value
        setSettingsToLocalSotrage()
    })



    const setting_fluid_gallery_list = ref(getInitialSettingFluidGalleryList())

    function getInitialSettingFluidGalleryList() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_fluid_gallery_list === undefined) {
            return false
        }
        return setting_object.setting_fluid_gallery_list
    }

    watch(setting_fluid_gallery_list, () => {
        settings.value.setting_fluid_gallery_list = setting_fluid_gallery_list.value
        setSettingsToLocalSotrage()
    })


    const setting_display_archived = ref(getInitialSettingDisplayArchived())
    function getInitialSettingDisplayArchived() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_display_archived === undefined) {
            return false
        }
        return setting_object.setting_display_archived
    }
    watch(setting_display_archived, () => {
        settings.value.setting_display_archived = setting_display_archived.value
        setSettingsToLocalSotrage()
    })


    const setting_display_card_element_v2 = ref(getInitialSettingDisplayCardElementV2())
    function getInitialSettingDisplayCardElementV2() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_display_card_element_v2 === undefined) {
            return false
        }
        return setting_object.setting_display_card_element_v2
    }
    watch(setting_display_card_element_v2, () => {
        settings.value.setting_display_card_element_v2 = setting_display_card_element_v2.value
        setSettingsToLocalSotrage()
    })
    



    const setting_activate_authenticatoion_token = ref(getInitialSettingActivateAuthenticationToken())
    function getInitialSettingActivateAuthenticationToken() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_activate_authenticatoion_token === undefined) {
            return false
        }
        return setting_object.setting_activate_authenticatoion_token
    }
    watch(setting_activate_authenticatoion_token, () => {
        settings.value.setting_activate_authenticatoion_token = setting_activate_authenticatoion_token.value
        setSettingsToLocalSotrage()
    })

    const setting_authenticatoion_token = ref(getInitialSettingAuthenticationToken())
    function getInitialSettingAuthenticationToken() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_authenticatoion_token === undefined) {
            return ''
        }
        return setting_object.setting_authenticatoion_token
    }
    watch(setting_authenticatoion_token, () => {
        settings.value.setting_authenticatoion_token = setting_authenticatoion_token.value
        setSettingsToLocalSotrage()
    })

    const setting_random_elo_rule = ref(getInitialRandomEloRule())
    function getInitialRandomEloRule() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_random_elo_rule === undefined) {
            return 'rule_2'
        }
        return setting_object.setting_random_elo_rule
    }
    watch(setting_random_elo_rule, () => {
        settings.value.setting_random_elo_rule = setting_random_elo_rule.value
        setSettingsToLocalSotrage()
    })

    const setting_random_elo_reload_gallery_after_count = ref(5)


    const setting_random_elo_static_exclude_id_list_count = ref(getRandomEloStaticExcludeIdListCount())
    function getRandomEloStaticExcludeIdListCount() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_random_elo_static_exclude_id_list_count === undefined) {
            return '0'
        }
        return setting_object.setting_random_elo_static_exclude_id_list_count
    }
    watch(setting_random_elo_static_exclude_id_list_count, () => {
        settings.value.setting_random_elo_static_exclude_id_list_count = setting_random_elo_static_exclude_id_list_count.value
        setSettingsToLocalSotrage()
    })



    const setting_random_elo_activate_filtering = ref(getRandomEloActivateFiltering())
    function getRandomEloActivateFiltering() {
        const setting_object = getSettingsFromLocalStorage()
        if (setting_object.setting_random_elo_activate_filtering === undefined) {
            return false
        }
        return setting_object.setting_random_elo_activate_filtering
    }
    watch(setting_random_elo_activate_filtering, () => {
        settings.value.setting_random_elo_activate_filtering = setting_random_elo_activate_filtering.value
        setSettingsToLocalSotrage()
    })
    

    return {
        setting_dark_mode,
        setting_show_preview_image_in_gallery_list,
        setting_show_background_in_gallery_list,
        setting_hightlight_never_played,
        setting_fluid_gallery_list,
        setting_activate_authenticatoion_token,
        setting_authenticatoion_token,
        setting_display_archived,
        setting_display_card_element_v2,
        setting_random_elo_rule,
        setting_random_elo_reload_gallery_after_count,
        setting_random_elo_static_exclude_id_list_count,
        setting_random_elo_activate_filtering,

        show_settings,
        resetSettings,
        reset,
    }
})