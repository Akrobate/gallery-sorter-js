 import { ref, type Ref } from 'vue'

import { defineStore } from 'pinia'
import { bookmark_repository } from '@/repositories'

export const useBookmarks = defineStore('bookmarks', () => {


  const bookmark_list: Ref<any[]> = ref([])


  async function loadBookmarks() {
    bookmark_list.value = await searchBookmarks({})
  }


  async function searchBookmarks(input: object = {}) {
    const {
      bookmark_list,
    } = await bookmark_repository.search({
      ...input,
    })
    return bookmark_list
  }

  /**
   * @param gallery_id 
   * @param filename 
   */
  async function createBookmarks(gallery_id: string, filename: string) {
    const response = await bookmark_repository.create(gallery_id, filename)
    await loadBookmarks()
    return response
  }


  /**
   * @param bookmark_id 
   */
  async function deleteBookmarks(bookmark_id: string) {
    const response = await bookmark_repository.delete(bookmark_id)
    await loadBookmarks()
    return response
  }


  /**
   * @param gallery_id 
   * @param filename 
   */
  async function checkBookmarkExists(gallery_id: string, filename: string | undefined) {
    const bookmark_list = await searchBookmarks({
      filename,
      gallery_id,
    })
    const found = bookmark_list
      .find((item: any) => item.gallery_id === gallery_id && item.filename === filename)

    return found === undefined ? false : true
  }

  async function tryToCreateBookmark(gallery_id: string, filename: string) {
    const exists = await checkBookmarkExists(gallery_id, filename)
    if (!exists) {
      await createBookmarks(gallery_id, filename)
    }
  }

  async function tryToRemoveBookmark(gallery_id: string, filename: string) {
    const bookmark_list = await searchBookmarks({
      filename,
      gallery_id,
    })
    const [
      bookmark,
    ] = bookmark_list
    if (bookmark) {
      await deleteBookmarks(bookmark.id)
    }
  }

  return {
    bookmark_list,

    searchBookmarks,
    createBookmarks,
    deleteBookmarks,
    checkBookmarkExists,
    tryToCreateBookmark,
    tryToRemoveBookmark,
    loadBookmarks,
  }
})
