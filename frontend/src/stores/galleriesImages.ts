import { ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import { image_repository } from '@/repositories'

export const useGalleriesImages = defineStore('galleriesImages', () => {
  
  const images: Ref<any[]> = ref([])

  async function searchImages(gallery_id: string) {
    const {
      image_list,
    } = await image_repository.searchImages(gallery_id)
    return image_list
  }

  return {
    images,
    searchImages,
  }
})
