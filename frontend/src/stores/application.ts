import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import { application_repository } from '@/repositories'
import { useDisplay } from 'vuetify'

export const useApplication = defineStore('Application', () => {

  const { name: display_name } = useDisplay()
  const is_small_screen = ref()
  is_small_screen.value = ['xs', 'sm'].includes(display_name.value)

  watch(display_name, () => {
    is_small_screen.value = ['xs', 'sm'].includes(display_name.value)
  })

  async function check() {
    const response = await application_repository.check()
    return response
  }


  async function refreshCaches() {
    const response = await application_repository.refreshCaches()
    return response
  }

  const isSmallScreen = computed(() => {
    const { name: display_name } = useDisplay()
    return ['xs', 'sm'].includes(display_name.value)
  })

  return {
    check,
    refreshCaches,
    isSmallScreen,
    is_small_screen,
  }
})
