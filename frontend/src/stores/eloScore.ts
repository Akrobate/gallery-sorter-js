import { ref } from 'vue'
import { defineStore } from 'pinia'
import { elo_score_repository } from '@/repositories'

export const useEloScore = defineStore('eloScore', () => {
  
  // ********* Preparing to move all elo stuff here *********
  const global_exclude_id_list = ref<any>([])
  
  const priority_lower_display_count_every = 2
  let priority_lower_display_count_every_counter = 0

  const image_list = ref<any>([])
  const results_count = ref<string>()
  const gallery_id = ref<string|null>(null)

  async function eloGetRandom(exclude_id_list: string[] = []):Promise<any> {
    if (gallery_id.value === null) {
      return
    }
    const result = await eloGetRandomRepository(
      gallery_id.value,
      {
        exclude_id_list: [].concat(<any>exclude_id_list, global_exclude_id_list.value),
          priority_lower_display_count: (priority_lower_display_count_every_counter % priority_lower_display_count_every) === 0
            ? true : false
      }
    )
    priority_lower_display_count_every_counter++

    global_exclude_id_list.value.push(result.id)

    if (global_exclude_id_list.value.length > (<any>results_count.value / 3)) {
      global_exclude_id_list.value.shift()
    }

    return result
  }


  async function loadEloResults() {
    if (gallery_id.value === null) {
      return
    }
    image_list.value = await eloResults(gallery_id.value)
    results_count.value = image_list.value.length
  }

  // ********************************************************

  async function initElo(gallery_id: string) {
    const {
      elo_score_list,
    } = await elo_score_repository.initElo(gallery_id)
    return elo_score_list
  }

  async function eloResults(gallery_id: string) {
    const {
      elo_score_list,
    } = await elo_score_repository.eloResults(gallery_id)
    return elo_score_list
  }


  async function eloGetRandomRepository(gallery_id: string, input: object) {
    const elo_score = await elo_score_repository.eloGetRandom(gallery_id, input)
    return elo_score
  }


  async function eloDuelResult(gallery_id: string, input: object) {
    const elo_score = await elo_score_repository.eloDuelResult(gallery_id, input)
    return elo_score
  }
  

  return {
    eloGetRandom,
    initElo,
    eloResults,
    eloGetRandomRepository,
    eloDuelResult,
    loadEloResults,
  }
})
