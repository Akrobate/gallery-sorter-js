import { ref } from 'vue'
import { defineStore } from 'pinia'
import { gallery_repository } from '@/repositories'

export const useTags = defineStore('tags', () => {

  const selected_tag_list = ref([])


  async function searchGalleryTags() {
    const {
      gallery_tag_list:tag_list,
    } = await gallery_repository.searchGalleryTags()
    return tag_list
  }

  return {
    selected_tag_list,
    searchGalleryTags,
  }
})
