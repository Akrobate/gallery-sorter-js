import { ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import { gallery_repository } from '@/repositories'

import { useEloScore } from '@/stores/eloScore'
import { useAppSettings } from '@/stores/appSettings'
import { useGalleriesImages } from '@/stores/galleriesImages'


export const useGalleries = defineStore('galleries', () => {
  
  const galleries: Ref<any[]> = ref([])

  const app_settings = useAppSettings()

  async function searchGalleries(input: object = {}) {
    const {
      gallery_list,
    } = await gallery_repository.searchGalleries({
      ...input,
      archived: app_settings.setting_display_archived ? undefined : false,
    })
    return gallery_list
  }

  async function searchGalleriesWithStatistics(input: object = {}) {
    const {
      gallery_list,
    } = await gallery_repository.searchGalleriesWithStatistics({
      ...input,
      archived: app_settings.setting_display_archived ? undefined : false,
    })
    return gallery_list
  }


  function formatGalleriesWithStatisticsRootCountValues(gallery_list : any = []) {
    const all_id = gallery_list.map((gallery: any) => gallery.actions.map((action: any) => action.id)).flat()
    const uniq_action_id_list = [
      ...new Set(all_id)
    ]
    return gallery_list.map((gallery: any) => {
      const _gallery = {
        ...gallery,
      }
      uniq_action_id_list.forEach((item : any) => {
        const _action = gallery.actions.find((act: any) => act.id === item)
        if (_action === undefined) {
          _gallery[`folder_${item}_count`] = 0
        } else {
          _gallery[`folder_${item}_count`] = _action.folder_file_count
        }
      })
      return _gallery
    })
  }


  async function getGallery(gallery_id: string) {
    const gallery = await gallery_repository.getGallery(gallery_id)
    return gallery
  }

  async function searchGalleryTags() {
    const {
      gallery_tag_list:tag_list,
    } = await gallery_repository.searchGalleryTags()
    return tag_list
  }
  
  async function processAction(gallery_id: string, action_id: string, filename: string) {
    const action = await gallery_repository.processAction(gallery_id, action_id, filename)
    return action
  }

  async function getGalleryCardData(gallery_id: string) {
    
    const elo_score_store = useEloScore()
    const galleries_images_store = useGalleriesImages()

    const elo_score_list = await elo_score_store.eloResults(gallery_id)
    let [
      best_image,
    ] = elo_score_list
    let elo_scores_count = elo_score_list.length

    const image_list = await galleries_images_store.searchImages(gallery_id)

    if (best_image === undefined) {
      [
        best_image,
      ] = image_list
      elo_scores_count = image_list.length
    }
  
    
    const total_played = elo_score_list.map((item: any) => item.display_count)
      .reduce((partial_sum: number, a: number) => partial_sum + a, 0)
    const never_played_count = elo_score_list.filter((item: any) => item.display_count === 0).length
    const average_played = (total_played / elo_scores_count).toFixed(2)
  
    return {
      elo_scores_count,
      total_played,
      average_played,
      never_played_count,
      best_image,
    }
  }



  function selectRandomGallery(_gallery_list : any, rule : string) {
    _gallery_list.sort((item_a: any, item_b: any) => {
      if (Number(item_a.average_played) < Number(item_b.average_played)) {
        return -1
      } else if (Number(item_a.average_played) > Number(item_b.average_played)) {
        return 1
      }
      return 0
    })

    let gallery_id = null

    const gallery_count = _gallery_list.length

    if (gallery_count <= 1) {
      return _gallery_list[0].id
    }

    switch (rule) {
      case 'rule_1':
        gallery_id = _gallery_list[Math.ceil(Math.random() * (gallery_count > 1 ? 2 : 1)) - 1].id
        break

      case 'rule_2':
        gallery_id = _gallery_list[Math.ceil(Math.random() * (gallery_count > 2 ? 3 : 1)) - 1].id
        break

      case 'rule_3':
        if (Math.random() > 0.5) {
          gallery_id = _gallery_list[0].id  
        } else {
          const selected_index = Math.ceil(Math.random() * gallery_count) - 1
          gallery_id = _gallery_list[selected_index].id
        }
        break

      case 'rule_4':
        gallery_id = _gallery_list[Math.ceil(Math.random() * gallery_count) - 1].id
        break
    }

    return gallery_id
  }


  return {
    galleries,
    searchGalleries,
    searchGalleriesWithStatistics,
    formatGalleriesWithStatisticsRootCountValues,
    getGallery,
    processAction,
    getGalleryCardData,
    searchGalleryTags,
    selectRandomGallery,
  }
})
