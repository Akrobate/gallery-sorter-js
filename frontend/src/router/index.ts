import { createRouter, createWebHistory } from 'vue-router'

import HomeView from '@/views/HomeView.vue'
import GalleriesListView from '@/views/GalleriesListView.vue'
import EloView from '@/views/EloView.vue'
import GenericImageView from '@/views/GenericImageView.vue'
import GenericImageListView from '@/views/GenericImageListView.vue'
import AuthorizationRequiredView from '@/views/AuthorizationRequiredView.vue'
import RandomGalleryEloView from '@/views/RandomGalleryEloView.vue'
import BookmarksView from '@/views/BookmarksView.vue'
import AboutView from '@/views/AboutView.vue'
import ErrorView from '@/views/ErrorView.vue'
import WorstImageListView from '@/views/WorstImageListView.vue'
import MobileAppLayout from '@/components/layouts/MobileAppLayout.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView,
    },
    {
      path: '/bookmarks',
      name: 'bookmarks',
      component: BookmarksView,
    },
    {
      path: '/random-gallery-elo',
      name: 'random-gallery-elo',
      component: RandomGalleryEloView,
    },
    {
      path: '/galleries',
      name: 'galleries',
      component: GalleriesListView,
    },
    {
      path: '/galleries/:gallery_id/images/:filename/',
      name: 'image',
      props: true,
      component: GenericImageView,
    },
    {
      path: '/galleries/:gallery_id/images',
      name: 'galleries-images',
      props: (route) => ({
        ...route.params,
        view_type: 'image',
      }),
      component: GenericImageListView,
    },
    {
      path: '/galleries/:gallery_id/elo',
      name: 'gallery-elo',
      props: true,
      component: EloView,
    },
    {
      path: '/galleries/:gallery_id/elo/images',
      name: 'gallery-elo-images',
      props: (route) => ({
        ...route.params,
        view_type: 'elo',
      }),
      component: GenericImageListView,
    },
    {
      path: '/galleries/:gallery_id/elo/images/:elo_score_id',
      name: 'elo-image',
      props: true,
      component: GenericImageView,
    },
    {
      path: '/unauthorized',
      name: 'unauthorized',
      props: false,
      component: AuthorizationRequiredView,
    },
    {
      path: '/error',
      name: 'error',
      props: false,
      component: ErrorView,
    },
    {
      path: '/worst-images',
      name: 'worst-images',
      props: false,
      component: WorstImageListView,
    },
    {
      path: '/galleries/:gallery_id/mobile-elo',
      name: 'gallery-mobile-elo',
      props: true,
      component: EloView,
      meta: {
        mobile: true,
        layout: MobileAppLayout,
      },
    },
    {
      path: '/mobile-random-gallery-elo',
      name: 'mobile-random-gallery-elo',
      props: false,
      component: RandomGalleryEloView,
      meta: {
        mobile: true,
        layout: MobileAppLayout,
      },
    },
  ]
})

export default router
