import router from '@/router'
import http_status from 'http-status'

import { storeToRefs } from 'pinia'
import { useAppSettings } from '@/stores/appSettings'

function getInterceptorFunctions() {
  const app_settings = useAppSettings()
  
  const {
    setting_activate_authenticatoion_token,
    setting_authenticatoion_token,
  } = storeToRefs(app_settings)
  
  const responseValidVoidInterceptor = (response: any) => response
  
  const responseUnauthorizedRedirect = (error: any) => {
    if (error.response.status === http_status.UNAUTHORIZED) {
      router.push({ name: 'unauthorized' })
    }
    return Promise.reject(error)
  }
  
  const requestAuthenticate = async (config: any) => {
    if (setting_activate_authenticatoion_token.value) {
      config.headers = {
        Authorization: `${setting_authenticatoion_token.value}`
      }
    }
    return config
  }
  
  return {
    responseValidVoidInterceptor,
    responseUnauthorizedRedirect,
    requestAuthenticate,
  }
}

export {
  getInterceptorFunctions
}