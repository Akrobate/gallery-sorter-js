import { Configuration } from '@/configuration'
import axios from 'axios'

const {
    API_URL,
} = Configuration

class EloScoreRepository {

    async initElo(gallery_id: string) {
        const response = await axios.post(
            `${API_URL}/api/v1/galleries/${gallery_id}/init-elo`
        )
        return response.data
    }

    async eloResults(gallery_id: string) {
        const response = await axios.get(
            `${API_URL}/api/v1/galleries/${gallery_id}/elo-results`
        )
        return response.data
    }

    async eloGetRandom(gallery_id: string, input: object) {
        const response = await axios.post(
            `${API_URL}/api/v1/galleries/${gallery_id}/elo-get-random`,
            input
        )
        return response.data
    }

    /**
     * 
     * @param gallery_id 
     * @param input {
     *      elo_score_id_1: string,
     *      elo_score_id_2: string,
     *      winner: number,
     *  }
     * @returns 
     */
    async eloDuelResult(
        gallery_id: string,
        input: any
    ) {
        
        const response = await axios.post(
            `${API_URL}/api/v1/galleries/${gallery_id}/elo-duel-result`,
            input
        )
        return response.data
    }
}

const elo_score_repository = new EloScoreRepository()

export {
    EloScoreRepository,
    elo_score_repository,
}