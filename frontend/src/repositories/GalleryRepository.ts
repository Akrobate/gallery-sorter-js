import { Configuration } from '@/configuration'
import axios from 'axios'

const {
    API_URL,
} = Configuration

class GalleryRepository {

    async searchGalleries(input: object = {}) {
        const response = await axios.get(`${API_URL}/api/v1/galleries`,
            {
                params: input,
            }
        )
        return response.data
    }


    async searchGalleriesWithStatistics(input: object = {}) {
        const response = await axios.get(`${API_URL}/api/v1/galleries-with-statistics`,
            {
                params: input,
            }
        )
        return response.data
    }


    async getGallery(gallery_id: string) {
        const response = await axios.get(`${API_URL}/api/v1/galleries/${gallery_id}`)
        return response.data
    }

    async processAction(gallery_id: string, action_id: string, filename: string) {
        const response = await axios.post(
            `${API_URL}/api/v1/galleries/${gallery_id}/process-action/${action_id}`,
            {
                filename: filename,
            }
        )
        return response.data
    }

    async searchGalleryTags() {
        const response = await axios.get(`${API_URL}/api/v1/galleries-tags`)
        return response.data
    }

}

const gallery_repository = new GalleryRepository()

export {
    GalleryRepository,
    gallery_repository,
}