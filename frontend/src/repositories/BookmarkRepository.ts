import { Configuration } from '@/configuration'
import axios from 'axios'

const {
    API_URL,
} = Configuration

class BookmarkRepository {

    async search(input: object = {}) {
        const response = await axios.get(`${API_URL}/api/v1/bookmarks`,
            {
                params: input,
            }
        )
        return response.data
    }


    async create(gallery_id: string, filename: string) {
        const response = await axios.post(
            `${API_URL}/api/v1/bookmarks`,
            {
                gallery_id,
                filename,
            }
        )
        return response.data
    }


    async delete(bookmark_id: string) {
        const response = await axios.delete(`${API_URL}/api/v1/bookmarks/${bookmark_id}`)
        return response.data
    }

}

const bookmark_repository = new BookmarkRepository()

export {
    BookmarkRepository,
    bookmark_repository,
}