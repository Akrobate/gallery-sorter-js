import { Configuration } from '@/configuration'
import axios from 'axios'

const {
    API_URL,
} = Configuration

class ImageRepository {

    async searchImages(gallery_id: string) {
        const response = await axios.get(`${API_URL}/api/v1/galleries/${gallery_id}/images`)
        return response.data
    }

}

const image_repository = new ImageRepository()

export {
    ImageRepository,
    image_repository,
}