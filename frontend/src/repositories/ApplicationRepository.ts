import { Configuration } from '@/configuration'
import axios from 'axios'

const {
    API_URL,
} = Configuration

class ApplicationRepository {

    async check() {
        const response = await axios.get(`${API_URL}/api/v1/application/check`)
        return response.data
    }

    async refreshCaches() {
        const response = await axios.get(`${API_URL}/api/v1/application/refresh-caches`)
        return response.data
    }
}

const application_repository = new ApplicationRepository()

export {
    ApplicationRepository,
    application_repository,
}
