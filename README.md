# gallery-sorter-js

## Principes

Gallery sorter does not use any database. All data files are stored in json folders in application_files.
The folder configured for application files must exist. In this folder, a folder .application will be automaticly created.

Files architecture:
* All available galleries myst be declared in file .application/galleries.json
* All the scoring results files will be automaticly created and updated

# Technical requirements

* node 16.14.2

## Setup your galleries

galleries_files_path describes the root path for your galleries folders. Gallery folder must be declared in galleries.json file and should be a subfolder of galleries_files_path.

application_files path describe the root path where the hidden application files will be created. Actually could be the same path than galleries path.


## Backend configuration

```yaml
server:
  port: 3000
  host: 'http://192.168.1.19'
  static_url: '/static'

storage:
  files:
    application_files: ../data/
    galleries_files_path: ../data/galleries/
    
frontend:
  port: 8080
  host: 'http://192.168.1.19'
```

## Frontend configuration

Please refer to ./frontend/README.md


## Configuration of galleries

galleries.json
```json
[
    {
        "id": "g1id",
        "name": "Gallery 1 (test)",
        "folder": "gallery_1/",
        "actions": [
            {
                "id": "REMOVE_ID",
                "name": "Remove",
                "icon": "mdi-delete-outline",
                "display": true,
                "action": "mv",
                "hotkey": "d",
                "folder": "trash"
            },
            {
                "id": "OFF_TOPIC_ID",
                "name": "Hors sujet",
                "icon": "mdi-file-question-outline",
                "display": true,
                "action": "mv",
                "hotkey": "o",
                "folder": "off-topic"
            },
            {
                "id": "NSFW_ID",
                "name": "NSFW",
                "icon": "mdi-alert-outline",
                "display": true,
                "action": "mv",
                "hotkey": "x",
                "folder": "nsfw"
            },
            {
                "id": "SELECTED_ID",
                "name": "Selected",
                "icon": "mdi-star",
                "display": true,
                "hotkey": "s",
                "action": "cp,mv",
                "folder": "../",
                "folder2": "selected"
            }
        ]
    }
]
```

## Configuration of actions

Params:

* id: Action ID must be unique on all instance
* name: Free string to name action
* icon: mdi icon class name
* display: True: will be displayed in Elo, false: will be groupped in '...'
* hotkey: Keyboard hot key to trigger action
* action: available: 'cp', 'mv', 'cp,mv'


Actions can be configured in one place and used as reference in galleries.
actions.json
```json
[
    {
        "id": "REMOVE_ID",
        "name": "Remove",
        "icon": "mdi-delete-outline",
        "display": false,
        "hotkey": "d",
        "action": "mv",
        "folder": "trash"
    },
    {
        "id": "OFF_TOPIC_ID",
        "name": "Hors sujet",
        "icon": "mdi-file-question-outline",
        "display": false,
        "hotkey": "o",
        "action": "mv",
        "folder": "off-topic"
    },
    {
        "id": "NSFW_ID",
        "name": "NSFW",
        "icon": "mdi-alert-outline",
        "display": true,
        "hotkey": "x",
        "action": "mv",
        "folder": "nsfw"
    }
]
```

If this file exists then galleries.json can look like:
galleries.json
```json
[
    {
        "id": "g1id",
        "name": "Gallery 1 (test)",
        "folder": "gallery_1/",
        "actions": [
            {
                "id": "REMOVE_ID",
            },
            {
                "id": "OFF_TOPIC_ID",
            },
            {
                "id": "NSFW_ID",
            }
        ]
    }
]
```


# Hotkeys

In galleries actions hotkeys can be cutomized

Default hotkeys

* **ArrowLeft**: Left select
* **ArrowRight**: Right select
* **ArrowUp**: Display miniatures
* **ArrowDown**: Hide miniatures
* **Enter**: Validate choise
* **ShiftRight**: Validate and keep choice
* **=**: Equality between propositions

Warning, Custom hotkeys cannot be the same as the default hotkeys


# Testing CI

## Triggering the CI

* Merging `develop` on `realease/canadidate` branch will automaticly triggre tests and lint on backend and frontend project.
* To trigger the CI only on frontend you can merge your `develop` on `frontend/release`
* To trigger the CI only on backend you can merge your `develop` on `backend/release`


# TODOS:

## new todos:

- [x] automate testing in backends
- [x] Write documentation about testing methodology in CI (frontend/release , backend/release, release/candidate)
- [x] Frontend Remove useless element folder for components
- [ ] Add helper for writing new entry in gallery.json
    * The helper should recursivly detect all folders that contains images
    * Once detected should suggest to add it to the gallery json file
    * each suggestion has a zone that show the json code to add to gallery json
    * nice to have, add all existing tags in helper zone to help tagging the entry
    * Pass to node 20 to be able to read recursivly folders
- [x] Find duplicates scripts creation
- [ ] Find duplicates scripts document
- [X] Create bookmark feature
    * Should be able to bookmark any image
    * Should be able to preview bookmark gallery as any other gallery
    * Should be able to preview the image of bookmark
    * All actions of gallery must be accessible


## Review todos:
* Add checkService
** Add mecanic to not start service if there is collision hotkey
** Add frontend test to check if there no ID duplications and collision hotkeys

* Refactoring front:
** Splitting in elements the preview image page

