'use strict';

const {
    configuration,
} = require('./configuration');

const express = require('express');
const routes = require('./routes');

const {
    AuthenticationMiddleware,
    error_manager_middleware,
    not_found_error_middleware,
    access_control_allow_middleware,
    express_urlencoded_middleware,
    cors_middleware,
    express_json,
} = require('./middlewares');

/* istanbul ignore next */
const authentication_middleware = new AuthenticationMiddleware(
    configuration.authentication.token
        ? configuration.authentication.token
        : null
);

const app = express();
app.disable('x-powered-by');

app.use(access_control_allow_middleware);
app.use(express_json);
app.use(express_urlencoded_middleware);
app.use(cors_middleware);

app.use(
    configuration.server.static_url,
    express.static(configuration.storage.files.galleries_files_path)
);
app.use(authentication_middleware.checkAuthentication());
app.use(routes.url_prefix, routes.api_routes);

app.use(not_found_error_middleware);
app.use(error_manager_middleware);

module.exports = {
    app,
    authentication_middleware,
};


