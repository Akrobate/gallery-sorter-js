'use strict';

const path = require('path');

const {
    FileRepository,
} = require('./FileRepository');

const {
    configuration,
} = require('../configuration');

class GalleryImageRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryImageRepository}
     */
    static getInstance() {
        if (GalleryImageRepository.instance === null) {
            GalleryImageRepository.instance = new GalleryImageRepository(
                FileRepository.getInstance()
            );
        }

        return GalleryImageRepository.instance;
    }

    /**
     * @param {FileRepository} file_repository
     */
    constructor(file_repository) {
        this.file_repository = file_repository;
    }

    /**
     * @param {string} directory
     * @returns {Promise<Array>}
     */
    async listFilesInDirectory(directory) {
        const file_list = await this.file_repository.listFilesInDirectory(directory);
        return file_list;
    }


    /**
     * @param {string} gallery_folder
     * @returns {Array}
     */
    async listFilesInGalleryFolder(gallery_folder) {
        const gallery_images_list = await this
            .listFilesInDirectory(
                path.join(configuration.storage.files.galleries_files_path, gallery_folder)
            );
        return gallery_images_list;
    }

}

GalleryImageRepository.instance = null;

module.exports = {
    GalleryImageRepository,
};
