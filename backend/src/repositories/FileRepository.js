'use strict';

const fs = require('fs');

class FileRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {FileRepository}
     */
    static getInstance() {
        if (FileRepository.instance === null) {
            FileRepository.instance = new FileRepository();
        }

        return FileRepository.instance;
    }

    /**
     * @param {Object} directory
     * @return {Array}
     */
    async listFilesInDirectory(directory) {
        const file_list = await fs.promises.readdir(directory, {
            withFileTypes: true,
        });
        return file_list
            .filter((dirent) => dirent.isFile())
            .map((item) => item.name);
    }

    /**
     * @param {string} file_path
     * @returns {Boolean}
     */
    exists(file_path) {
        const exists = fs.existsSync(file_path);
        return exists;
    }

    /**
     * @param {*} from_file
     * @param {*} to_file
     * @return {Object}
     */
    async moveFile(from_file, to_file) {
        const result = await fs.promises
            .rename(from_file, to_file);
        return result;
    }


    /**
     * @param {string} file
     * @param {string} content
     * @returns {void}
     */
    async appendFile(file, content) {
        const result = await fs.promises.appendFile(file, content);
        return result;
    }


    /**
     * @param {Object} directory
     * @return {Array}
     */
    async recursiveReadDirectory(directory) {
        const result = await fs.promises.readdir(
            directory,
            {
                recursive: true,
                withFileTypes: true,
            }
        );
        return result;
    }

}

FileRepository.instance = null;

module.exports = {
    FileRepository,
};
