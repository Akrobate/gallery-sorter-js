'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');


class GalleryTagRepository {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryTagRepository}
     */
    static getInstance() {
        if (GalleryTagRepository.instance === null) {
            GalleryTagRepository.instance = new GalleryTagRepository(
                new JsonFileRepository()
            );
        }

        return GalleryTagRepository.instance;
    }


    /**
     * @param {*} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'galleries_tags.json';
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search() {
        const results = await this.json_file_repository.search(this.repository_filename);
        return results;
    }

}

GalleryTagRepository.instance = null;

module.exports = {
    GalleryTagRepository,
};
