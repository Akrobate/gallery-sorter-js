'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');


class PromptRepository {


    /**
     * @static
     * @returns {PromptRepository}
     */
    static getInstance() {
        if (PromptRepository.instance === null) {
            PromptRepository.instance = new PromptRepository(
                new JsonFileRepository()
            );
        }

        return PromptRepository.instance;
    }


    /**
     * @param {*} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'prompts.json';
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search() {
        const results = await this.json_file_repository.search(this.repository_filename);
        return results;
    }

    /**
     * @param {*} input
     * @returns {Object}
     */
    async create(input) {
        const result = await this.json_file_repository.create(
            this.repository_filename,
            input
        );
        return result;
    }


    /**
     * @param {*} input
     * @returns {Object}
     */
    async createMany(input) {
        const result = await this.json_file_repository.createMany(
            this.repository_filename,
            input
        );
        return result;
    }


    /**
     * @param {String} id
     * @param {Object} input
     * @returns {Object}
     */
    async update(id, input) {
        const result = await this.json_file_repository.update(
            this.repository_filename,
            id,
            input
        );
        return result;
    }


}

PromptRepository.instance = null;

module.exports = {
    PromptRepository,
};
