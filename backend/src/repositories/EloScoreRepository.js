'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');

/**
 * model: {
 *      id:
 *      filename,
 *      elo_score,
 *      display_count,
 *      created_timestamp
 *      updated_timestamp
 * },
 */


class EloScoreRepository {


    /**
     * @static
     */
    static get ELO_JOSN_FILE_PREFIX() {
        return 'elo_score_';
    }

    /* istanbul ignore next */
    /**
     * @static
     * @returns {EloScoreRepository}
     */
    static getInstance() {
        if (EloScoreRepository.instance === null) {
            EloScoreRepository.instance = new EloScoreRepository(
                JsonFileRepository.getInstance()
            );
        }

        return EloScoreRepository.instance;
    }


    /**
     * @param {JsonFileRepository} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
    }


    /**
     * @param {String} gallery_id
     * @returns {Array}
     */
    async getEloRanking(gallery_id) {
        const elo_score_list = await this
            .search(
                gallery_id,
                {},
                {
                    sort: {
                        elo_score: -1,
                    },
                }
            );
        return elo_score_list;
    }


    /**
     * @param {*} gallery_id
     * @param {*} input
     * @param {*} options
     * @returns {Array<Object>}
     */
    async search(gallery_id, input, options = {}) {
        const elo_score_list = await this.json_file_repository.search(
            `${EloScoreRepository.ELO_JOSN_FILE_PREFIX}${gallery_id}.json`,
            input,
            options
        );
        return elo_score_list;
    }


    /**
     * @param {*} gallery_id
     * @param {*} input
     * @returns {Object}
     */
    async create(gallery_id, input) {
        const result = await this.json_file_repository.create(
            `${EloScoreRepository.ELO_JOSN_FILE_PREFIX}${gallery_id}.json`,
            input
        );
        return result;
    }


    /**
     * @param {String} gallery_id
     * @param {String} id
     * @param {Object} input
     * @returns {Object}
     */
    async update(gallery_id, id, input) {
        const result = await this.json_file_repository.update(
            `${EloScoreRepository.ELO_JOSN_FILE_PREFIX}${gallery_id}.json`,
            id,
            input
        );
        return result;
    }


    /**
     * @param {String} gallery_id
     * @param {String} id
     * @returns {Object}
     */
    async delete(gallery_id, id) {
        const result = await this.json_file_repository.delete(
            `${EloScoreRepository.ELO_JOSN_FILE_PREFIX}${gallery_id}.json`,
            id
        );
        return result;
    }
}

EloScoreRepository.instance = null;

module.exports = {
    EloScoreRepository,
};
