'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');


/**
 * [
 *   {
 *       "id": "g1id",
 *       "name": "Gallery 1 (test)",
 *       "folder": "gallery_1/",
 *       "actions": [
 *           {
 *               "id": "REMOVE_ID",
 *               "name": "Remove",
 *               "icon": "mdi-delete-outline",
 *               "display": true,
 *               "action": "mv",
 *               "hotkey": "d",
 *               "folder": "trash"
 *           },
 *           {
 *               "id": "OFF_TOPIC_ID",
 *               "name": "Hors sujet",
 *               "icon": "mdi-file-question-outline",
 *               "display": true,
 *               "action": "mv",
 *               "hotkey": "o",
 *               "folder": "off-topic"
 *           },
 *           {
 *               "id": "NSFW_ID",
 *               "name": "NSFW",
 *               "icon": "mdi-alert-outline",
 *               "display": true,
 *               "hotkey": "x",
 *               "action": "mv",
 *               "folder": "nsfw"
 *           }
 *       ]
 *   }
 * ]
 */

class GalleryRepository {


    /**
     * @static
     * @returns {GalleryRepository}
     */
    static getInstance() {
        if (GalleryRepository.instance === null) {
            GalleryRepository.instance = new GalleryRepository(
                new JsonFileRepository()
            );
        }

        return GalleryRepository.instance;
    }


    /**
     * @param {*} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'galleries.json';

        this.data = [];
        this.data_is_loaded = false;
    }


    /**
     * @param {Bool} force_reload
     * @return {Promise<Array>}
     */
    async loadData(force_reload = false) {
        if (!this.data_is_loaded || force_reload) {
            this.data = await this.json_file_repository.search(this.repository_filename);
            this.data_is_loaded = true;
        }
        return this.data;
    }

    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search(input = {}) {

        const {
            tag_list,
            force_reload,
            archived,
        } = input;

        const results = await this.loadData(force_reload === true);
        return results
            .map((item) => this.formatGalleryItem(item))
            .filter((item) => {
                if (archived === undefined) {
                    return true;
                }
                const item_archive = item.archived === undefined ? false : item.archived;
                return item_archive === archived;
            })
            .filter((item) => {
                let match = true;
                if (tag_list !== undefined) {
                    tag_list.forEach((tag_query) => {
                        if (!item.tags.includes(tag_query)) {
                            match = false;
                        }
                    });
                }
                return match;
            });
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    async get(id) {
        const results = await this.loadData();
        return this.formatGalleryItem(
            results.find((item) => item.id === id)
        );
    }


    /**
     * @param {Object} gallery
     * @returns {Object}
     */
    formatGalleryItem(gallery) {

        const output = {
            ...gallery,
        };
        const key_types_list = [
            {
                name: 'id',
                type: 'string',
            },
            {
                name: 'name',
                type: 'string',
            },
            {
                name: 'folder',
                type: 'string',
            },
            {
                name: 'tags',
                type: 'array',
            },
            {
                name: 'actions',
                type: 'array',
            },
            {
                name: 'display',
                type: 'boolean',
                default: true,
            },
        ];

        key_types_list.forEach((item) => {
            if (output[item.name] === undefined) {
                if (item.type === 'string') {
                    output[item.name] = '';
                }
                if (item.type === 'array') {
                    output[item.name] = [];
                }
                if (item.type === 'boolean') {
                    /* istanbul ignore next */
                    output[item.name] = item.default === undefined ? true : item.default;
                }
            }
        });
        return output;
    }
}

GalleryRepository.instance = null;

module.exports = {
    GalleryRepository,
};
