'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');

/**
 * model: {
 * },
 */


class BookmarkRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {BookmarkRepository}
     */
    static getInstance() {
        if (BookmarkRepository.instance === null) {
            BookmarkRepository.instance = new BookmarkRepository(
                JsonFileRepository.getInstance()
            );
        }

        return BookmarkRepository.instance;
    }


    /**
     * @param {JsonFileRepository} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'bookmarks.json';

    }

    /**
     * @param {*} input
     * @param {*} options
     * @returns {Array<Object>}
     */
    async search(input, options = {}) {
        const bookmark_list = await this.json_file_repository.search(
            this.repository_filename,
            input,
            options
        );
        return bookmark_list;
    }


    /**
     * @param {*} input
     * @returns {Object}
     */
    async create(input) {
        const result = await this.json_file_repository.create(
            this.repository_filename,
            input
        );
        return result;
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    async delete(id) {
        const result = await this.json_file_repository.delete(
            this.repository_filename,
            id
        );
        return result;
    }
}

BookmarkRepository.instance = null;

module.exports = {
    BookmarkRepository,
};
