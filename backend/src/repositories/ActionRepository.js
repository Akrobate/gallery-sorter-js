'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');

/**
 * [
 *           {
 *               "id": "REMOVE_ID",
 *               "name": "Remove",
 *               "icon": "mdi-delete-outline",
 *               "action": "mv",
 *               "hotkey": "d",
 *               "folder": "trash"
 *           },
 *           {
 *               "id": "OFF_TOPIC_ID",
 *               "name": "Hors sujet",
 *               "icon": "mdi-file-question-outline",
 *               "action": "mv",
 *               "hotkey": "o",
 *               "folder": "off-topic"
 *           },
 *           {
 *               "id": "NSFW_ID",
 *               "name": "NSFW",
 *               "icon": "mdi-alert-outline",
 *               "hotkey": "x",
 *               "action": "mv",
 *               "folder": "nsfw"
 *           }
 * ]
 */

class ActionRepository {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ActionRepository}
     */
    static getInstance() {
        if (ActionRepository.instance === null) {
            ActionRepository.instance = new ActionRepository(
                new JsonFileRepository()
            );
        }

        return ActionRepository.instance;
    }


    /**
     * @param {JsonFileRepository} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'actions.json';

        this.data = [];
        this.data_is_loaded = false;
    }


    /**
     * @param {Bool} force_reload
     * @return {Promise<Array>}
     */
    async loadData(force_reload = false) {
        if (!this.data_is_loaded || force_reload) {
            this.data = await this.json_file_repository.search(this.repository_filename);
            this.data_is_loaded = true;
        }
        return this.data;
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search() {
        const results = await this.loadData();
        return results;
    }


    /**
     * @param {Number} id
     * @returns {Object}
     */
    async get(id) {
        const results = await this.loadData();
        console.log(results);
        return results.find((item) => item.id === id);
    }


    /**
     * @param {Object} action
     * @return {Object}
     */
    async enrichAction(action) {
        const base_action = await this.get(action.id);
        console.log('base_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_action');

        console.log(base_action);
        console.log('base_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_actionbase_action');
        if (base_action !== undefined) {
            return {
                ...base_action,
                ...action,
            };
        }
        return action;
    }
}

ActionRepository.instance = null;

module.exports = {
    ActionRepository,
};
