'use strict';

const {
    ActionRepository,
} = require('./ActionRepository');
const {
    BookmarkRepository,
} = require('./BookmarkRepository');
const {
    GalleryRepository,
} = require('./GalleryRepository');
const {
    GalleryImageRepository,
} = require('./GalleryImageRepository');
const {
    EloScoreRepository,
} = require('./EloScoreRepository');
const {
    GalleryTagRepository,
} = require('./GalleryTagRepository');
const {
    FileRepository,
} = require('./FileRepository');
const {
    ImageMetaDataRepository,
} = require('./ImageMetaDataRepository');
const {
    PromptRepository,
} = require('./PromptRepository');

module.exports = {
    ActionRepository,
    BookmarkRepository,
    FileRepository,
    GalleryRepository,
    GalleryTagRepository,
    GalleryImageRepository,
    EloScoreRepository,
    ImageMetaDataRepository,
    PromptRepository,
};
