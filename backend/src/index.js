/* istanbul ignore file */

'use strict';

const {
    app,
} = require('./app');

const {
    logger,
} = require('./logger');

const {
    configuration,
} = require('./configuration');

app.listen(configuration.server.port, (error) => {
    if (error) {
        logger.error(error);
        process.exit(1); // eslint-disable-line no-process-exit
    }
    logger.info(`Listening on port ${configuration.server.port}`);
});
