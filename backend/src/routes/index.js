'use strict';

const {
    Router,
} = require('express');

const {
    GalleryController,
    GalleryTagController,
    GalleryImageController,
    EloSorterController,
    ApplicationController,
    BookmarkController,
    PromptController,
} = require('../controllers');


const api_routes = Router(); // eslint-disable-line new-cap

const gallery_controller = GalleryController.getInstance();
const gallery_tag_controller = GalleryTagController.getInstance();
const gallery_image_controller = GalleryImageController.getInstance();
const elo_sorter_controller = EloSorterController.getInstance();
const application_controller = ApplicationController.getInstance();
const bookmark_controller = BookmarkController.getInstance();
const prompt_controller = PromptController.getInstance();

const url_prefix = '/api/v1';

// ************ Technical Application ************
api_routes.get(
    '/application/check',
    (request, response, next) => application_controller
        .check(request, response)
        .catch(next)
);

api_routes.get(
    '/application/refresh-caches',
    (request, response, next) => application_controller
        .refreshCaches(request, response)
        .catch(next)
);

api_routes.get(
    '/application/find-unknown-galleries',
    (request, response, next) => application_controller
        .findUnknowGalleryFolder(request, response)
        .catch(next)
);


// *************** Galleries ***************
api_routes.get(
    '/galleries',
    (request, response, next) => gallery_controller
        .search(request, response)
        .catch(next)
);

api_routes.get(
    '/galleries-with-statistics',
    (request, response, next) => gallery_controller
        .searchWithStatistics(request, response)
        .catch(next)
);

api_routes.get(
    '/galleries/:gallery_id',
    (request, response, next) => gallery_controller
        .read(request, response)
        .catch(next)
);

api_routes.post(
    '/galleries/:gallery_id/process-action/:action_id',
    (request, response, next) => gallery_controller
        .processAction(request, response)
        .catch(next)
);


// *************** Galleries Tags ***************
api_routes.get(
    '/galleries-tags',
    (request, response, next) => gallery_tag_controller
        .search(request, response)
        .catch(next)
);


// *************** Images ***************
api_routes.get(
    '/galleries/:gallery_id/images',
    (request, response, next) => gallery_image_controller
        .search(request, response)
        .catch(next)
);


// *************** ELO Algo ***************
api_routes.post(
    '/galleries/:gallery_id/init-elo',
    (request, response, next) => elo_sorter_controller
        .initEloRanking(request, response)
        .catch(next)
);

api_routes.get(
    '/galleries/:gallery_id/elo-results',
    (request, response, next) => elo_sorter_controller
        .getEloRanking(request, response)
        .catch(next)
);

api_routes.post(
    '/galleries/:gallery_id/elo-get-random',
    (request, response, next) => elo_sorter_controller
        .getRandom(request, response)
        .catch(next)
);

api_routes.post(
    '/galleries/:gallery_id/elo-duel-result',
    (request, response, next) => elo_sorter_controller
        .processEloDuelResult(request, response)
        .catch(next)
);


// ************ Bookmarks ******************
api_routes.post(
    '/bookmarks',
    (request, response, next) => bookmark_controller
        .create(request, response)
        .catch(next)
);

api_routes.get(
    '/bookmarks',
    (request, response, next) => bookmark_controller
        .search(request, response)
        .catch(next)
);

api_routes.delete(
    '/bookmarks/:id',
    (request, response, next) => bookmark_controller
        .delete(request, response)
        .catch(next)
);

// ************ Prompts *****************
api_routes.get(
    '/prompts',
    (request, response, next) => prompt_controller
        .search(request, response)
        .catch(next)
);

module.exports = {
    api_routes,
    url_prefix,
};
