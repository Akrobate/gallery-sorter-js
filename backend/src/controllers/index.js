'use strict';

const {
    GalleryController,
} = require('./GalleryController');
const {
    GalleryTagController,
} = require('./GalleryTagController');
const {
    GalleryImageController,
} = require('./GalleryImageController');
const {
    EloSorterController,
} = require('./EloSorterController');
const {
    ApplicationController,
} = require('./ApplicationController');
const {
    BookmarkController,
} = require('./BookmarkController');
const {
    PromptController,
} = require('./PromptController');

module.exports = {
    ApplicationController,
    BookmarkController,
    EloSorterController,
    GalleryController,
    GalleryImageController,
    GalleryTagController,
    PromptController,
};
