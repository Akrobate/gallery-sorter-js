'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    ActionService,
    GalleryService,
} = require('../services');

class GalleryController extends AbstractController {

    /**
     * @param {GalleryService} gallery_service
     * @param {ActionService} action_service
     */
    constructor(
        gallery_service,
        action_service
    ) {
        super();
        this.gallery_service = gallery_service;
        this.action_service = action_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryController}
     */
    static getInstance() {
        if (GalleryController.instance === null) {
            GalleryController.instance = new GalleryController(
                GalleryService.getInstance(),
                ActionService.getInstance()
            );
        }

        return GalleryController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async processAction(request, response) {
        const {
            gallery_id,
            action_id,
        } = request.params;
        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        filename: joi.string()
                            .trim()
                            .min(5)
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const action_result = await this.action_service.processAction(
            {
                gallery_id,
                action_id,
                filename: value.body.filename,
            }
        );

        return response.status(HTTP_CODE.CREATED).send(action_result);
    }


    /**
     * @param {*} request
     * @returns {Object}
     */
    validateSearchRequest(request) {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi
                                    .number()
                                    .required()
                            )
                            .optional(),
                        tag_list: joi
                            .array()
                            .items(
                                joi
                                    .string()
                                    .required()
                            )
                            .optional(),
                        archived: joi
                            .boolean()
                            .optional(),
                        sort_list: joi
                            .array()
                            .items(
                                joi
                                    .string()
                                    .required()
                            )
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = this.validateSearchRequest(request);

        this.checkValidationError(error);

        const data = await this.gallery_service.search(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            gallery_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async searchWithStatistics(request, response) {

        const {
            error,
            value,
        } = this.validateSearchRequest(request);

        this.checkValidationError(error);

        const data = await this.gallery_service.searchWithStatistics(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            gallery_list: data,
        });
    }

    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async read(request, response) {

        const {
            gallery_id,
        } = request.params;

        const data = await this.gallery_service.read(
            {
                gallery_id,
            }
        );
        return response.status(HTTP_CODE.OK).send(data);
    }

}

GalleryController.instance = null;

module.exports = {
    GalleryController,
};
