'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    GalleryTagService,
} = require('../services');

class GalleryTagController extends AbstractController {

    /**
     * @param {GalleryService} gallery_tag_service
     */
    constructor(
        gallery_tag_service
    ) {
        super();
        this.gallery_tag_service = gallery_tag_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryTagController}
     */
    static getInstance() {
        if (GalleryTagController.instance === null) {
            GalleryTagController.instance = new GalleryTagController(
                GalleryTagService.getInstance()
            );
        }

        return GalleryTagController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi
                                    .number()
                                    .required()
                            )
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const data = await this.gallery_tag_service.search(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            gallery_tag_list: data,
        });
    }

}

GalleryTagController.instance = null;

module.exports = {
    GalleryTagController,
};
