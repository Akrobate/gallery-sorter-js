'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    PromptService,
} = require('../services');

class PromptController extends AbstractController {

    /**
     * @param {PromptService} prompt_service
     */
    constructor(
        prompt_service
    ) {
        super();
        this.prompt_service = prompt_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {PromptController}
     */
    static getInstance() {
        if (PromptController.instance === null) {
            PromptController.instance = new PromptController(
                PromptService.getInstance()
            );
        }

        return PromptController.instance;
    }


    /**
     * @param {*} request
     * @returns {Object}
     */
    validateSearchRequest(request) {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi
                                    .number()
                                    .required()
                            )
                            .optional(),
                        sort_list: joi
                            .array()
                            .items(
                                joi
                                    .string()
                                    .required()
                            )
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = this.validateSearchRequest(request);

        this.checkValidationError(error);

        const data = await this.prompt_service.search(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            gallery_list: data,
        });
    }

}

PromptController.instance = null;

module.exports = {
    PromptController,
};
