'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    EloSorterService,
} = require('../services');


class EloSorterController extends AbstractController {

    /**
     * @param {EloSorterService} elo_sorter_service
     */
    constructor(
        elo_sorter_service
    ) {
        super();
        this.elo_sorter_service = elo_sorter_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {EloSorterController}
     */
    static getInstance() {
        if (EloSorterController.instance === null) {
            EloSorterController.instance = new EloSorterController(
                EloSorterService.getInstance()
            );
        }

        return EloSorterController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async initEloRanking(request, response) {
        const {
            gallery_id,
        } = request.params;

        const elo_score_list = await this.elo_sorter_service
            .initEloRanking(
                {
                    gallery_id,
                }
            );

        return response.status(HTTP_CODE.CREATED).send({
            elo_score_list,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getEloRanking(request, response) {

        const {
            gallery_id,
        } = request.params;

        const elo_score_list = await this.elo_sorter_service
            .getEloRanking(
                {
                    gallery_id,
                }
            );

        return response.status(HTTP_CODE.OK).send({
            elo_score_list,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async processEloDuelResult(request, response) {

        const {
            gallery_id,
        } = request.params;

        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        elo_score_id_1: joi.string()
                            .required(),
                        elo_score_id_2: joi.string()
                            .required(),
                        winner: joi.number()
                            .valid(1, 2, 0)
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const elo_result_list = await this.elo_sorter_service
            .processEloDuelResult(
                {
                    gallery_id,
                    elo_score_id_1: value.body.elo_score_id_1,
                    elo_score_id_2: value.body.elo_score_id_2,
                    winner: value.body.winner,
                }
            );

        return response.status(HTTP_CODE.OK).send({
            elo_result_list,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getRandom(request, response) {
        const {
            gallery_id,
        } = request.params;

        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        exclude_id_list: joi.array()
                            .items(
                                joi.string()
                            )
                            .optional(),
                        priority_lower_display_count: joi
                            .boolean()
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const random = await this.elo_sorter_service
            .getRandom(
                {
                    gallery_id,
                    exclude_id_list: value.body.exclude_id_list,
                    priority_lower_display_count: value.body.priority_lower_display_count,
                }
            );

        return response.status(HTTP_CODE.OK).send(random);
    }

}

EloSorterController.instance = null;

module.exports = {
    EloSorterController,
};
