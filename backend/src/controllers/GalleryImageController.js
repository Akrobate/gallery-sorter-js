'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    GalleryImageService,
} = require('../services');

class GalleryImageController extends AbstractController {

    /**
     * @param {GalleryImageService} gallery_image_service
     */
    constructor(
        gallery_image_service
    ) {
        super();
        this.gallery_image_service = gallery_image_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryImageController}
     */
    static getInstance() {
        if (GalleryImageController.instance === null) {
            GalleryImageController.instance = new GalleryImageController(
                GalleryImageService.getInstance()
            );
        }

        return GalleryImageController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi
                                    .number()
                                    .required()
                            )
                            .optional(),
                        campaign_status_list: joi
                            .array()
                            .items(
                                joi
                                    .number()
                                    .required()
                            )
                            .optional(),
                        sort_list: joi
                            .array()
                            .items(
                                joi
                                    .string()
                                    .required()
                            )
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const data = await this.gallery_image_service.search({
            ...value.query,
            gallery_id: request.params.gallery_id,
        });

        return response.status(HTTP_CODE.OK).send({
            image_list: data,
        });
    }

}

GalleryImageController.instance = null;

module.exports = {
    GalleryImageController,
};
