'use strict';

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    ApplicationService,
    CheckConfigurationService,
} = require('../services');

class ApplicationController extends AbstractController {

    /**
     * @param {ApplicationService} application_service
     * @param {CheckConfigurationService} check_configuration_service
     */
    constructor(
        application_service,
        check_configuration_service
    ) {
        super();
        this.application_service = application_service;
        this.check_configuration_service = check_configuration_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ApplicationController}
     */
    static getInstance() {
        if (ApplicationController.instance === null) {
            ApplicationController.instance = new ApplicationController(
                ApplicationService.getInstance(),
                CheckConfigurationService.getInstance()
            );
        }

        return ApplicationController.instance;
    }


    /**
     * @param {express.Request} _
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async check(_, response) {
        const result = await this.application_service.check();
        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {express.Request} _
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async refreshCaches(_, response) {
        const result = await this.application_service.refreshCaches();
        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {express.Request} _
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async findUnknowGalleryFolder(_, response) {
        const result = await this.check_configuration_service.findUnknowGalleryFolder();
        return response.status(HTTP_CODE.OK).send(result);
    }

}

ApplicationController.instance = null;

module.exports = {
    ApplicationController,
};
