'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    BookmarkService,
} = require('../services');


class BookmarkController extends AbstractController {

    /**
     * @param {BookmarkService} bookmark_service
     */
    constructor(
        bookmark_service
    ) {
        super();
        this.bookmark_service = bookmark_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {BookmarkController}
     */
    static getInstance() {
        if (BookmarkController.instance === null) {
            BookmarkController.instance = new BookmarkController(
                BookmarkService.getInstance()
            );
        }

        return BookmarkController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async delete(request, response) {
        const {
            id,
        } = request.params;

        await this.bookmark_service
            .delete(
                {
                    id,
                }
            );

        return response.status(HTTP_CODE.OK).send({
            deleted: true,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async create(request, response) {
        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        gallery_id: joi.string()
                            .required(),
                        filename: joi.string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);

        const result = await this.bookmark_service
            .create(
                {
                    gallery_id: value.body.gallery_id,
                    filename: value.body.filename,
                }
            );

        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {*} request
     * @returns {Object}
     */
    validateSearchRequest(request) {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        gallery_id: joi
                            .string()
                            .optional(),
                        filename: joi
                            .string()
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {
        const {
            error,
            value,
        } = this.validateSearchRequest(request);

        this.checkValidationError(error);

        const bookmark_list = await this.bookmark_service
            .search(value.query);

        return response.status(HTTP_CODE.OK).send({
            bookmark_list,
        });
    }

}

BookmarkController.instance = null;

module.exports = {
    BookmarkController,
};
