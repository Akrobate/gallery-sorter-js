'use strict';

const {
    CustomError,
} = require('../CustomError');

class AuthenticationMiddleware {

    /* istanbul ignore next */
    /**
     * @param {String} authentication_token
     */
    constructor(authentication_token = null) {
        this.authentication_token = authentication_token;
    }

    /**
     * @returns {Function}
     */
    checkAuthentication() {
        return (request, _response, next) => {
            if (this.authentication_token === null) {
                return next();
            }
            const request_authorization_token = request.get('Authorization');
            if (request_authorization_token !== this.authentication_token) {
                throw new CustomError(CustomError.UNAUTHORIZED);
            }
            return next();
        };
    }
}

module.exports = {
    AuthenticationMiddleware,
};

