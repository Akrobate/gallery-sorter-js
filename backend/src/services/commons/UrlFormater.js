'use strict';

const {
    configuration,
} = require('../../configuration');

class UrlFormater {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {UrlFormater}
     */
    static getInstance() {
        if (UrlFormater.instance === null) {
            UrlFormater.instance = new UrlFormater();
        }
        return UrlFormater.instance;
    }


    /**
     * @param {Object} gallery
     * @param {String} filename
     * @returns {string}
     */
    formatGalleryImageUrl(gallery, filename) {
        const {
            host,
            port,
        } = configuration.server;
        return `${host}:${port}${configuration.server.static_url}/${gallery.folder}/${filename}`;
    }

}

UrlFormater.instance = null;

module.exports = {
    UrlFormater,
};
