'use strict';

const {
    EloCalculator,
} = require('./EloCalculator');
const {
    UrlFormater,
} = require('./UrlFormater');
module.exports = {
    EloCalculator,
    UrlFormater,
};
