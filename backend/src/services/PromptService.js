'use strict';

const {
    PromptRepository,
    ImageMetaDataRepository,
} = require('../repositories');


class PromptService {

    /**
     * @param {PromptRepository} prompt_repository
     * @param {PromptRepository} image_meta_data_repository
     */
    constructor(
        prompt_repository,
        image_meta_data_repository
    ) {
        this.prompt_repository = prompt_repository;
        this.image_meta_data_repository = image_meta_data_repository;
    }

    /* istanbul ignore next */
    /**
     * @static
     * @returns {PromptService}
     */
    static getInstance() {
        if (PromptService.instance === null) {
            PromptService.instance = new PromptService(
                PromptRepository.getInstance(),
                ImageMetaDataRepository.getInstance()
            );
        }
        return PromptService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(input) {
        const result_list = await this.prompt_repository.search({
            ...input,
        });

        return result_list;
    }


    /**
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async calculateUniquePrompts() {

        const image_meta_data_list = await this.image_meta_data_repository.search();

        const image_meta_data_list_with_key = image_meta_data_list.map((item) => ({
            ...item,
            prompt_key: `${item.prompt} - ${item.negative_prompt}`,
        }));

        const uniq_promt_key_list = [
            ...new Set(image_meta_data_list_with_key.map((item) => item.prompt_key)),
        ];

        const uniq_prompts_with_gallery = [];

        uniq_promt_key_list.forEach((prompt_key) => {
            const _gallery_id_list = [];
            const galleries_filtered = image_meta_data_list_with_key
                .filter((item) => prompt_key === item.prompt_key);

            galleries_filtered.forEach((item) => {
                if (!_gallery_id_list
                    .map((_item) => _item.gallery_id)
                    .includes(item.gallery_id)
                ) {
                    _gallery_id_list.push({
                        gallery_id: item.gallery_id,
                        'Clip skip': item['Clip skip'],
                        Model: item.Model,
                    });
                }
            });
            uniq_prompts_with_gallery.push({
                prompt: galleries_filtered[0].prompt,
                negative_prompt: galleries_filtered[0].negative_prompt,
                gallery_id_list: _gallery_id_list,
            });
        });

        return uniq_prompts_with_gallery;
    }


    /**
     * @returns {Promise<void>}
     */
    async generateUniquePrompt() {
        const unique_prompts = await this.calculateUniquePrompts();

        // Check propt exists
        const prompt_list = await this.prompt_repository.search();

        const promt_to_create = [];

        unique_prompts.forEach((item) => {
            if (!prompt_list.map((_item) => _item.prompt_key).includes(item.prompt_key)) {
                promt_to_create.push(item);
            }
        });


        await this.prompt_repository.createMany(promt_to_create);
    }


    /**
     * @returns {Promise<void>}
     */
    async calculateUniqueWords() {
        const prompt_list = await this.prompt_repository.search();

        const unique_words = [];

        prompt_list.forEach((unique_prompt) => {

            const word_list = this.normalizePromptToWords(unique_prompt.prompt);

            word_list.forEach((word) => {
                if (word === '') {
                    return;
                }
                const index = unique_words.findIndex((item) => item.word === word);

                if (index === -1) {
                    unique_words.push({
                        word,
                        count: unique_prompt.gallery_id_list.length,
                    });
                } else {
                    unique_words[index].count += unique_prompt.gallery_id_list.length;
                }
            });

        });

        unique_words.sort((a, b) => b.count - a.count);

        return unique_words;
    }

    /**
     *
     * @param {String} prompt
     * @returns {Array}
     */
    normalizePromptToWords(prompt) {
        const words = prompt.split(',');
        const normalized_words = words.map((word) => {
            let word_parts = word
                .replaceAll('(', '')
                .replaceAll(')', '')
                .replaceAll('<', '')
                .replaceAll('>', '')
                .split(':');

            word_parts = word_parts.map((item) => item.trim());

            if (word_parts[0] === 'lora') {
                return `${word_parts[0]}:${word_parts[1]}`;
            }

            return word_parts[0];
        });
        return normalized_words;
    }


}

PromptService.instance = null;

module.exports = {
    PromptService,
};
