'use strict';

const path = require('path');

const fs = require('fs');
const {
    configuration,
} = require('../configuration');


const {
    CustomError,
} = require('../CustomError');

const {
    GalleryRepository,
    ActionRepository,
} = require('../repositories');


class ActionService {

    /**
     * @param {GalleryRepository} gallery_repository
     * @param {ActionRepository} action_repository
     */
    constructor(
        gallery_repository,
        action_repository
    ) {
        this.gallery_repository = gallery_repository;
        this.action_repository = action_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ActionService}
     */
    static getInstance() {
        if (ActionService.instance === null) {
            ActionService.instance = new ActionService(
                GalleryRepository.getInstance(),
                ActionRepository.getInstance()
            );
        }
        return ActionService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async processAction(input) {

        const {
            gallery_id,
            action_id,
            filename,
        } = input;

        const gallery = await this.gallery_repository.get(gallery_id);

        const {
            actions,
        } = gallery;

        const gallery_action = actions.find((item) => item.id === action_id);
        const action = await this.action_repository.enrichAction(gallery_action);

        if (action.action === 'mv') {
            await this.moveFile(gallery.folder, action.folder, filename);
        }
        if (action.action === 'cp,mv') {
            await this.copyFile(gallery.folder, action.folder, filename);
            await this.moveFile(gallery.folder, action.folder2, filename);
        }

        return {
            status: 'Action processed',
        };
    }


    /**
     * @param {string} gallery_relative_folder_source
     * @param {string} gallery_relative_folder_destination
     * @param {string} filename
     * @returns {void}
     */
    async moveFile(
        gallery_relative_folder_source,
        gallery_relative_folder_destination,
        filename
    ) {
        const {
            source_folder,
            destination_folder,
        } = this.formatSourceAndDestinationFolders(
            gallery_relative_folder_source,
            gallery_relative_folder_destination
        );

        /* istanbul ignore next */
        if (path.isAbsolute(destination_folder)) {
            this.checkExists(destination_folder);
        } else {
            await this.createDestinationFolderIfNotExists(destination_folder);
        }

        await fs.promises.rename(
            path.join(source_folder, filename),
            path.join(destination_folder, filename)
        );
    }


    /**
     * @param {string} gallery_relative_folder_source
     * @param {string} gallery_relative_folder_destination
     * @param {string} filename
     * @returns {void}
     */
    async copyFile(
        gallery_relative_folder_source,
        gallery_relative_folder_destination,
        filename
    ) {
        const {
            source_folder,
            destination_folder,
        } = this.formatSourceAndDestinationFolders(
            gallery_relative_folder_source,
            gallery_relative_folder_destination
        );

        this.checkExists(path.join(source_folder, filename));

        /* istanbul ignore next */
        if (path.isAbsolute(destination_folder)) {
            this.checkExists(destination_folder);
        } else {
            await this.createDestinationFolderIfNotExists(destination_folder);
        }

        await fs.promises.copyFile(
            path.join(source_folder, filename),
            path.join(destination_folder, filename)
        );
    }


    /**
     * @param {*} gallery_relative_folder_source
     * @param {*} gallery_relative_folder_destination
     * @returns {Object}
     */
    formatSourceAndDestinationFolders(
        gallery_relative_folder_source,
        gallery_relative_folder_destination
    ) {
        const source_folder = path.join(
            configuration.storage.files.galleries_files_path,
            gallery_relative_folder_source
        );

        let destination_folder = path.join(
            source_folder,
            gallery_relative_folder_destination
        );

        /* istanbul ignore next */
        if (path.isAbsolute(gallery_relative_folder_destination)) {
            destination_folder = gallery_relative_folder_destination;
        }

        return {
            source_folder,
            destination_folder,
        };
    }


    /**
     * @param {string} source_folder
     * @param {string} filename
     * @returns {void}
     */
    checkExists(source_folder) {
        const source_file_exists = fs.existsSync(source_folder);
        if (!source_file_exists) {
            throw new CustomError(CustomError.NOT_FOUND);
        }
    }


    /**
     * @param {string} destination_folder
     * @returns {void}
     */
    async createDestinationFolderIfNotExists(destination_folder) {
        const destination_folder_exists = fs.existsSync(destination_folder);
        if (!destination_folder_exists) {
            await fs.promises.mkdir(destination_folder);
        }
    }
}

ActionService.instance = null;

module.exports = {
    ActionService,
};
