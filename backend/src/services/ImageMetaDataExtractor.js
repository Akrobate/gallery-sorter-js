'use strict';

const path = require('path');
const exifr = require('exifr');
const {
    configuration,
} = require('../configuration');

const {
    GalleryService,
} = require('./GalleryService');

const {
    GalleryRepository,
    ActionRepository,
    FileRepository,
    ImageMetaDataRepository,
    GalleryImageRepository,
} = require('../repositories');

class ImageMetaDataExtractor {

    /**
     * @param {GalleryService} gallery_service
     * @param {GalleryRepository} gallery_repository
     * @param {ActionRepository} action_repository
     * @param {FileRepository} file_repository
     * @param {GalleryImageRepository} gallery_image_repository
     * @param {ImageMetaDataRepository} image_meta_data_repository
     */
    constructor(
        gallery_service,
        gallery_repository,
        action_repository,
        file_repository,
        gallery_image_repository,
        image_meta_data_repository
    ) {
        this.gallery_service = gallery_service;
        this.gallery_repository = gallery_repository;
        this.action_repository = action_repository;
        this.file_repository = file_repository;
        this.gallery_image_repository = gallery_image_repository;
        this.image_meta_data_repository = image_meta_data_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ImageMetaDataExtractor}
     */
    static getInstance() {
        if (ImageMetaDataExtractor.instance === null) {
            ImageMetaDataExtractor.instance = new ImageMetaDataExtractor(
                GalleryService.getInstance(),
                GalleryRepository.getInstance(),
                ActionRepository.getInstance(),
                FileRepository.getInstance(),
                GalleryImageRepository.getInstance(),
                ImageMetaDataRepository.getInstance()
            );
        }
        return ImageMetaDataExtractor.instance;
    }


    /**
     * @returns {Promise}
     */
    async processCreateUpdateImagesMetaData() {

        const galleries_path = configuration.storage.files.galleries_files_path;
        const gallery_list = await this.gallery_repository.search();

        const image_meta_data_list = await this.image_meta_data_repository.search();

        for (const [index, gallery] of gallery_list.entries()) {

            console.log(`${gallery.id} ${index} / ${gallery_list.length}`);

            const data_bucket_to_save = [];

            const images_list = await this.gallery_image_repository
                .listFilesInGalleryFolder(gallery.folder);

            for (const image of images_list) {

                const image_path = path.join(gallery.folder, image);

                let sd_meta_data = {};

                try {
                    sd_meta_data = this.parseStableDiffusionData(await exifr.parse(path.join(
                        galleries_path,
                        image_path
                    )));
                } catch (error) {
                    console.log('No exif data');
                }

                const meta_data = {
                    gallery_id: gallery.id,
                    gallery_name: gallery.name,
                    image_path,
                    prompt: sd_meta_data.prompt ? sd_meta_data.prompt : '',
                    negative_prompt: sd_meta_data.negative_prompt ? sd_meta_data.negative_prompt : '',
                    Size: sd_meta_data.options ? sd_meta_data.options.Size : '',
                    Model: sd_meta_data.options ? sd_meta_data.options.Model : '',
                    'Clip skip': sd_meta_data.options ? sd_meta_data.options['Clip skip'] : '',
                };

                const found_saved_meta_data = image_meta_data_list
                    .find((item) => item.image_path === meta_data.image_path);

                if (!found_saved_meta_data) {
                    data_bucket_to_save.push(meta_data);
                }
            }

            await this.image_meta_data_repository.createMany(data_bucket_to_save);
        }

        const updated_image_meta_data_list = await this.image_meta_data_repository.search();
        return {
            count: updated_image_meta_data_list.length,
        };
    }


    /**
     * @param {String} metadata
     * @returns {Object}
     */
    parseStableDiffusionData(metadata) {

        if (!metadata.parameters) {
            throw new Error();
        }

        let prompt = '';
        let negative_prompt = '';
        let options_string = '';


        const metadata_splitted_params = metadata.parameters.split('\n');

        if (metadata_splitted_params.length === 3) {
            ([
                prompt,
                negative_prompt,
                options_string,
            ] = metadata_splitted_params);
        } else {
            ([
                prompt,
                options_string,
            ] = metadata_splitted_params);
        }

        let working_options_string = options_string;
        const regex = /(Lora hashes|TI hashes): "([^"]+)"/g;

        const found = options_string.match(regex);
        if (found !== null && found.length > 0) {
            working_options_string = working_options_string.replace(found[0], '');
        }
        const options = {};

        working_options_string.split(',')
            .forEach((item) => {
                const property = item.split(':');
                if (property.length !== 2) {
                    return null;
                }
                const [
                    key,
                    value,
                ] = item.split(':');

                options[key.trim()] = value.trim();
                return null;
            });
        if (options.Steps) {
            options.Steps = Number(options.Steps);
        }
        options['Clip skip'] = options['Clip skip'] ? Number(options['Clip skip']) : 1;

        return {
            prompt,
            negative_prompt: negative_prompt
                .replace('Negative prompt: ', '')
                .replace('Negative prompt:', ''),
            options,
        };
    }

}

ImageMetaDataExtractor.instance = null;

module.exports = {
    ImageMetaDataExtractor,
};
