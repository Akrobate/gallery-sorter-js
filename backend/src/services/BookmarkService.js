'use strict';

const {
    BookmarkRepository,
    GalleryRepository,
} = require('../repositories');

const {
    UrlFormater,
} = require('./commons');

class BookmarkService {

    /**
     * @param {BookmarkRepository} bookmark_repository
     * @param {GalleryRepository} gallery_repository
     * @param {UrlFormater} url_formater
     */
    constructor(
        bookmark_repository,
        gallery_repository,
        url_formater
    ) {
        this.bookmark_repository = bookmark_repository;
        this.gallery_repository = gallery_repository;
        this.url_formater = url_formater;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {BookmarkService}
     */
    static getInstance() {
        if (BookmarkService.instance === null) {
            BookmarkService.instance = new BookmarkService(
                BookmarkRepository.getInstance(),
                GalleryRepository.getInstance(),
                UrlFormater.getInstance()
            );
        }
        return BookmarkService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(input) {
        const gallery_list = await this.gallery_repository.search();
        const bookmark_list = await this.bookmark_repository.search({
            ...input,
        });

        const result_list = [];
        for (const item of bookmark_list) {
            const gallery = gallery_list
                .find((_gallery) => _gallery.id === item.gallery_id);
            if (gallery) {
                result_list.push({
                    ...item,
                    gallery,
                    url: this.url_formater.formatGalleryImageUrl(gallery, item.filename),
                });
            } else {
                await this.delete(item);
            }
        }
        return result_list;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async create(input) {
        const result = await this.bookmark_repository.create({
            ...input,
            bookmared_timestamp_millis: Date.now(),
        });
        return result;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async delete(input) {
        const {
            id,
        } = input;
        const result = await this.bookmark_repository.delete(id);
        return result;
    }

}

BookmarkService.instance = null;

module.exports = {
    BookmarkService,
};
