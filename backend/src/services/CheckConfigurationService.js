'use strict';

const {
    configuration,
} = require('../configuration');

const {
    GalleryRepository,
    GalleryTagRepository,
    ActionRepository,
    FileRepository,
} = require('../repositories');


class CheckConfigurationService {

    /**
     * Constructor
     * @param {GalleryRepository} gallery_repository
     * @param {GalleryTagRepository} gallery_tag_repository
     * @param {ActionRepository} action_repository
     * @param {FileRepository} file_repository
     */
    constructor(
        gallery_repository,
        gallery_tag_repository,
        action_repository,
        file_repository
    ) {
        this.gallery_repository = gallery_repository;
        this.gallery_tag_repository = gallery_tag_repository;
        this.action_repository = action_repository;
        this.file_repository = file_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CheckConfigurationService}
     */
    static getInstance() {
        if (CheckConfigurationService.instance === null) {
            CheckConfigurationService.instance = new CheckConfigurationService(
                GalleryRepository.getInstance(),
                GalleryTagRepository.getInstance(),
                ActionRepository.getInstance(),
                FileRepository.getInstance()
            );
        }
        return CheckConfigurationService.instance;
    }


    /**
     * @return {Object}
     */
    async findUnknowGalleryFolder() {
        const formated = (await this.file_repository
            .recursiveReadDirectory(configuration.storage.files.galleries_files_path)
        )
            .filter((item) => item.name.includes('.png'))
            .map((item) => `${item.path.replace(configuration.storage.files.galleries_files_path, '')}/`)
            .filter((item) => item !== '/');

        const action_list = await this.action_repository.search();

        /* istanbul ignore next */
        const action_folders = [...new Set(action_list.map((_i) => [_i.folder, _i.folder2]).flat())]
            .filter((item) => item)
            .filter((item) => !item.includes('/'));

        const gallery_folder_list = (await this.gallery_repository.search())
            .map((item) => item.folder);

        const filtered_action_folders = [...new Set(formated)].filter((item) => {
            let _result = true;
            /* istanbul ignore next */
            action_folders.forEach((action_folder) => {
                _result = item.includes(action_folder) ? false : _result;
            });
            /* istanbul ignore next */
            _result = gallery_folder_list.includes(item) ? false : _result;
            return _result;
        });

        return filtered_action_folders;
    }

}

CheckConfigurationService.instance = null;

module.exports = {
    CheckConfigurationService,
};
