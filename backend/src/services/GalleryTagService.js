'use strict';

const {
    GalleryRepository,
    GalleryTagRepository,
} = require('../repositories');


class GalleryTagService {

    /**
     * Constructor
     * @param {GalleryRepository} gallery_repository
     * @param {GalleryTagRepository} gallery_tag_repository
     */
    constructor(
        gallery_repository,
        gallery_tag_repository
    ) {
        this.gallery_repository = gallery_repository;
        this.gallery_tag_repository = gallery_tag_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryTagService}
     */
    static getInstance() {
        if (GalleryTagService.instance === null) {
            GalleryTagService.instance = new GalleryTagService(
                GalleryRepository.getInstance(),
                GalleryTagRepository.getInstance()
            );
        }
        return GalleryTagService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search() {
        const gallery_list = await this.gallery_repository.search();
        const gallery_tag_list = await this.gallery_tag_repository.search();

        const tag_list = [];
        gallery_list.forEach((gallery) => {
            gallery.tags.forEach((gallery_tag) => {
                if (tag_list.find((tag) => tag.name === gallery_tag) === undefined) {

                    const external_data_tag = gallery_tag_list
                        .find((item) => item.name === gallery_tag);

                    if (external_data_tag) {
                        tag_list.push(external_data_tag);
                    } else {
                        tag_list.push({
                            name: gallery_tag,
                            color: null,
                            category: null,
                        });
                    }
                }
            });
        });


        return tag_list;
    }


    /**
     * @param {String} path
     * @returns {Array}
     */
    async suggestTagsFromPath(path) {

        const tag_list = await this.search();

        const _normalise = (_str) => _str
            .replaceAll('/', ' ')
            .replaceAll('_', ' ')
            .replaceAll('-', ' ')
            .replaceAll(':', ' ')
            .replaceAll('  ', ' ')
            .replaceAll('  ', ' ')
            .toLowerCase();

        const tag_list_result = [];
        tag_list.map((item) => _normalise(item.name))
            .forEach((value, index) => {
                if (_normalise(path).includes(value)) {
                    tag_list_result.push(tag_list[index].name);
                }
            });

        return tag_list_result;
    }

}

GalleryTagService.instance = null;

module.exports = {
    GalleryTagService,
};
