'use strict';

const {
    configuration,
} = require('../configuration');

const {
    GalleryService,
} = require('./GalleryService');

const {
    GalleryRepository,
    ActionRepository,
    FileRepository,
} = require('../repositories');

class ApplicationService {

    /**
     * @param {GalleryService} gallery_service
     * @param {GalleryRepository} gallery_repository
     * @param {ActionRepository} action_repository
     * @param {FileRepository} file_repository
     */
    constructor(
        gallery_service,
        gallery_repository,
        action_repository,
        file_repository
    ) {
        this.gallery_service = gallery_service;
        this.gallery_repository = gallery_repository;
        this.action_repository = action_repository;
        this.file_repository = file_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ApplicationService}
     */
    static getInstance() {
        if (ApplicationService.instance === null) {
            ApplicationService.instance = new ApplicationService(
                GalleryService.getInstance(),
                GalleryRepository.getInstance(),
                ActionRepository.getInstance(),
                FileRepository.getInstance()
            );
        }
        return ApplicationService.instance;
    }


    /**
     * @returns {Object}
     */
    async check() {
        let status = 'ok';
        let message = '';

        const duplicates_id_list = await this.findGalleryIdDuplication();
        if (duplicates_id_list.length > 0) {
            status = 'duplicate_error';
            message = duplicates_id_list.join(',');
        }

        return {
            status,
            message,
        };
    }

    /**
     * @returns {Array}
     */
    async findGalleryIdDuplication() {
        const all_galleries_list = await this.gallery_service.search({});
        const duplicates_id_list = this.findDuplicatesId(
            all_galleries_list.map((gallery) => gallery.id)
        );
        return duplicates_id_list;
    }


    /**
     * @returns {Array}
     */
    async findGalleryActionHotKeysCollision() {
        const reserved_keys = [
            'r',
        ];
        const all_galleries_list = await this.gallery_service.search({});
        const actions_gallery_list = all_galleries_list.map((gallery) => gallery.actions);
        let duplication_detected = false;

        actions_gallery_list.forEach((action_list) => {
            const hotkey_list = action_list.map((action) => action.hotkey);
            reserved_keys.forEach((reserved_key) => {
                if (hotkey_list.includes(reserved_key)) {
                    duplication_detected = true;
                }
            });
        });
        return duplication_detected;
    }

    /**
     * @returns {Object}
     */
    async refreshCaches() {
        await this.gallery_repository.loadData(true);
        await this.action_repository.loadData(true);
        return {
            status: 'ok',
        };
    }


    /**
     * @param {*} id_list
     * @return {Object}
     */
    findDuplicatesId(id_list) {
        const duplication_list = [];
        id_list.forEach((id) => {
            const index = duplication_list.findIndex((item) => item.id === id);
            if (index < 0) {
                duplication_list.push({
                    id,
                    count: 1,
                });
            } else {
                duplication_list[index].count++;
            }
        });
        return duplication_list
            .filter((item) => item.count > 1)
            .map((item) => item.id);
    }

}

ApplicationService.instance = null;

module.exports = {
    ApplicationService,
};
