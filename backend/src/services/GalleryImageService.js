'use strict';

const {
    UrlFormater,
} = require('./commons');

const {
    GalleryImageRepository,
    GalleryRepository,
} = require('../repositories');

class GalleryImageService {

    /**
     * @param {UrlFormater} url_formater
     * @param {GalleryImageRepository} gallery_image_repository
     * @param {GalleryRepository} gallery_repository
     */
    constructor(
        url_formater,
        gallery_image_repository,
        gallery_repository
    ) {
        this.url_formater = url_formater;
        this.gallery_image_repository = gallery_image_repository;
        this.gallery_repository = gallery_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryImageService}
     */
    static getInstance() {
        if (GalleryImageService.instance === null) {
            GalleryImageService.instance = new GalleryImageService(
                UrlFormater.getInstance(),
                GalleryImageRepository.getInstance(),
                GalleryRepository.getInstance()
            );
        }
        return GalleryImageService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(input) {
        const {
            gallery_id,
        } = input;
        const gallery = await this.gallery_repository.get(gallery_id);
        const gallery_images_list = await this.gallery_image_repository
            .listFilesInGalleryFolder(gallery.folder);

        return gallery_images_list.map((gallery_image) => ({
            url: this.url_formater.formatGalleryImageUrl(gallery, gallery_image),
            filename: gallery_image,
        }));
    }

}

GalleryImageService.instance = null;

module.exports = {
    GalleryImageService,
};
