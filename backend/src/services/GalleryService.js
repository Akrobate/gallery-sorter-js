'use strict';

const path = require('path');

const {
    UrlFormater,
} = require('./commons');

const {
    GalleryRepository,
    ActionRepository,
    EloScoreRepository,
    GalleryImageRepository,
} = require('../repositories');


class GalleryService {

    /**
     * @param {UrlFormater} url_formater
     * @param {GalleryRepository} gallery_repository
     * @param {ActionRepository} action_repository
     * @param {EloScoreRepository} elo_score_repository
     * @param {GalleryImageRepository} gallery_image_repository
     */
    constructor(
        url_formater,
        gallery_repository,
        action_repository,
        elo_score_repository,
        gallery_image_repository
    ) {
        this.url_formater = url_formater;
        this.gallery_repository = gallery_repository;
        this.action_repository = action_repository;
        this.elo_score_repository = elo_score_repository;
        this.gallery_image_repository = gallery_image_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {GalleryService}
     */
    static getInstance() {
        if (GalleryService.instance === null) {
            GalleryService.instance = new GalleryService(
                UrlFormater.getInstance(),
                GalleryRepository.getInstance(),
                ActionRepository.getInstance(),
                EloScoreRepository.getInstance(),
                GalleryImageRepository.getInstance()
            );
        }
        return GalleryService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async read(input) {
        const {
            gallery_id,
        } = input;

        const gallery = await this.gallery_repository.get(gallery_id);
        return this.enrichGalleryWithActions(gallery);
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(input) {
        const gallery_list = await this.gallery_repository.search({
            ...input,
        });
        const _enriched_gallery_list = await Promise.all(
            gallery_list.map((gallery) => this.enrichGalleryWithActions(gallery))
        );
        return _enriched_gallery_list;
    }


    /**
     * @param {Object} input
     * @returns {Array}
     */
    async searchWithStatistics(input) {
        const gallery_list = await this.search({
            ...input,
        });
        let _enriched_gallery_list = [];

        _enriched_gallery_list = await Promise.all(
            gallery_list.map((gallery) => this.enrichGalleryWithStatistics(gallery))
        );

        _enriched_gallery_list = await Promise.all(
            _enriched_gallery_list
                .map((gallery) => this.enrichGalleryActionsWithFolderFileCount(gallery))
        );
        return _enriched_gallery_list;
    }


    /**
     * @param {Object} gallery
     * @returns {Object}
     */
    async enrichGalleryWithStatistics(gallery) {
        const _gallery = {
            ...gallery,
        };

        const {
            id: gallery_id,
        } = gallery;

        const elo_score_list = await this.elo_score_repository.getEloRanking(gallery_id);
        let [
            best_image,
        ] = elo_score_list;
        let elo_scores_count = elo_score_list.length;

        const image_list = await this.gallery_image_repository
            .listFilesInGalleryFolder(gallery.folder);

        if (best_image === undefined) {
            const [
                first_image_filename,
            ] = image_list;
            best_image = {
                filename: first_image_filename,
            };
            elo_scores_count = image_list.length;
        }

        const total_played = elo_score_list.map((item) => item.display_count)
            .reduce((partial_sum, a) => partial_sum + a, 0);
        const never_played_count = elo_score_list.filter((item) => item.display_count === 0).length;
        const average_played = (total_played / elo_scores_count).toFixed(2);

        return {
            ..._gallery,
            elo_scores_count,
            total_played,
            average_played,
            never_played_count,
            best_image: {
                ...best_image,
                url: this.url_formater.formatGalleryImageUrl(gallery, best_image.filename),
            },
        };
    }


    /**
     * @param {Object} gallery
     * @returns {Object}
     */
    async enrichGalleryWithActions(gallery) {
        const _gallery = {
            ...gallery,
        };

        const {
            actions,
        } = _gallery;

        if (actions === undefined) {
            return _gallery;
        }

        for (const [index, item] of actions.entries()) {
            _gallery.actions[index] = await this.action_repository.enrichAction(item);
        }
        return _gallery;
    }


    /**
     * @param {Object} gallery
     * @returns {Object}
     */
    async enrichGalleryActionsWithFolderFileCount(gallery) {

        const _gallery = {
            ...gallery,
        };

        const {
            actions,
        } = _gallery;

        for (const [index, item] of actions.entries()) {
            let folder_file_count = 0;
            const folder_to_list = path.join(_gallery.folder, item.folder);
            try {
                const file_list = await this.gallery_image_repository
                    .listFilesInGalleryFolder(folder_to_list);
                folder_file_count = file_list.length;
            } catch (error) {
                console.log(`no folder ${folder_to_list}`);
            }

            _gallery.actions[index] = {
                ...item,
                folder_file_count,
            };
        }
        return _gallery;
    }
}

GalleryService.instance = null;

module.exports = {
    GalleryService,
};
