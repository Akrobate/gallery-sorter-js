'use strict';

const path = require('path');

const {
    configuration,
} = require('../configuration');

const {
    EloCalculator,
    UrlFormater,
} = require('./commons');

const {
    EloScoreRepository,
    GalleryRepository,
    GalleryImageRepository,
} = require('../repositories');

class EloSorterService {

    /**
     * @param {EloCalculator} elo_calculator
     * @param {UrlFormater} url_formater
     * @param {EloScoreRepository} elo_score_repository
     * @param {GalleryRepository} gallery_repository
     * @param {GalleryImageRepository} gallery_image_repository
     */
    constructor(
        elo_calculator,
        url_formater,
        elo_score_repository,
        gallery_repository,
        gallery_image_repository
    ) {
        this.elo_calculator = elo_calculator;
        this.url_formater = url_formater;
        this.elo_score_repository = elo_score_repository;
        this.gallery_repository = gallery_repository;
        this.gallery_image_repository = gallery_image_repository;
    }


    /**
     * @static
     */
    static get DEFAULT_SCORE() {
        return 1000;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {EloSorterService}
     */
    static getInstance() {
        if (EloSorterService.instance === null) {
            EloSorterService.instance = new EloSorterService(
                EloCalculator.getInstance(),
                UrlFormater.getInstance(),
                EloScoreRepository.getInstance(),
                GalleryRepository.getInstance(),
                GalleryImageRepository.getInstance()
            );
        }
        return EloSorterService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async initEloRanking(input) {
        const {
            gallery_id,
        } = input;
        const gallery = await this.gallery_repository.get(gallery_id);

        const gallery_images_list = await this.gallery_image_repository
            .listFilesInDirectory(
                path.join(configuration.storage.files.galleries_files_path, gallery.folder)
            );

        const elo_score_list = await this.elo_score_repository.search(gallery_id);

        const elo_score_to_create_list = gallery_images_list
            .filter((filename) => elo_score_list
                .find((elo_score) => elo_score.filename === filename) === undefined
            );

        const elo_score_created_list = [];
        for (const score_filename of elo_score_to_create_list) {
            // eslint-disable-next-line no-await-in-loop
            const score_created = await this.elo_score_repository.create(
                gallery_id,
                {
                    filename: score_filename,
                    elo_score: EloSorterService.DEFAULT_SCORE,
                    display_count: 0,
                    created_timestamp: Math.floor(Date.now() / 1000),
                    updated_timestamp: Math.floor(Date.now() / 1000),
                }
            );
            elo_score_created_list.push(score_created);
        }

        const elo_score_to_remove_list = elo_score_list
            .filter((elo_score) => !gallery_images_list.includes(elo_score.filename));

        for (const elo_score of elo_score_to_remove_list) {
            // eslint-disable-next-line no-await-in-loop
            await this.elo_score_repository.delete(
                gallery_id,
                elo_score.id
            );
        }

        return elo_score_created_list;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async getEloRanking(input) {
        const {
            gallery_id,
        } = input;

        const elo_score_list = await this.elo_score_repository
            .getEloRanking(gallery_id);

        const gallery = await this.gallery_repository.get(gallery_id);
        return elo_score_list.map((elo_score) => ({
            ...elo_score,
            url: this.url_formater.formatGalleryImageUrl(gallery, elo_score.filename),
        }));
    }


    /**
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async getRandom(input) {
        const {
            gallery_id,
            exclude_id_list,
            priority_lower_display_count,
        } = input;

        let elo_score_list = await this.elo_score_repository.search(
            gallery_id,
            {}
        );

        if (exclude_id_list) {
            elo_score_list = elo_score_list
                .filter((elo_score) => !exclude_id_list.includes(elo_score.id));
        }

        if (priority_lower_display_count) {

            const min_display_count = Math.min(...elo_score_list.map((item) => item.display_count));

            const lower_display_count_elo_socre_list = elo_score_list
                .filter((elo_score) => elo_score.display_count === min_display_count);

            /* istanbul ignore else */
            if (lower_display_count_elo_socre_list.length > 0) {
                elo_score_list = lower_display_count_elo_socre_list;
            }
        }
        const random_index = Math.ceil(Math.random() * elo_score_list.length) - 1;

        const gallery = await this.gallery_repository.get(gallery_id);
        return {
            ...elo_score_list[random_index],
            url: this.url_formater
                .formatGalleryImageUrl(gallery, elo_score_list[random_index].filename),
        };
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async processEloDuelResult(input) {

        const {
            gallery_id,
            elo_score_id_1,
            elo_score_id_2,
            winner,
        } = input;

        const elo_score_list = await this.elo_score_repository.search(
            gallery_id,
            {}
        );

        const elo_score_1 = elo_score_list
            .find((item) => item.id === elo_score_id_1);
        const elo_score_2 = elo_score_list
            .find((item) => item.id === elo_score_id_2);


        let elo_score_1_new_score = null;
        let elo_score_2_new_score = null;

        if (winner === 0) {
            elo_score_1_new_score = this.elo_calculator
                .getScorePlayerEgality(elo_score_1.elo_score, elo_score_2.elo_score);
            elo_score_2_new_score = this.elo_calculator
                .getScorePlayerEgality(elo_score_2.elo_score, elo_score_1.elo_score);
        } else if (winner === 1) {
            elo_score_1_new_score = this.elo_calculator
                .getScorePlayerWins(elo_score_1.elo_score, elo_score_2.elo_score);
            elo_score_2_new_score = this.elo_calculator
                .getScorePlayerLose(elo_score_2.elo_score, elo_score_1.elo_score);
        } else {
            elo_score_1_new_score = this.elo_calculator
                .getScorePlayerLose(elo_score_1.elo_score, elo_score_2.elo_score);
            elo_score_2_new_score = this.elo_calculator
                .getScorePlayerWins(elo_score_2.elo_score, elo_score_1.elo_score);
        }

        const elo_score_1_update_result = await this.elo_score_repository
            .update(
                gallery_id,
                elo_score_1.id,
                {
                    elo_score: elo_score_1_new_score,
                    display_count: elo_score_1.display_count + 1,
                }
            );

        const elo_score_2_update_result = await this.elo_score_repository
            .update(
                gallery_id,
                elo_score_2.id,
                {
                    elo_score: elo_score_2_new_score,
                    display_count: elo_score_2.display_count + 1,
                }
            );

        return [
            elo_score_1_update_result,
            elo_score_2_update_result,
        ];
    }


    /**
     * @param {*} all_propositions_list
     * @return {Array}
     */
    groupArrayByDisplayCount(all_propositions_list) {

        all_propositions_list.sort((a, b) => {
            if (a.display_count > b.display_count) {
                return -1;
            }
            return 1;
        });

        const grouped_by_display_count_propositions = [];
        let last_display_count = null;
        let proposition_group = [];

        all_propositions_list.forEach((proposition) => {
            if (
                last_display_count === null
                || last_display_count === proposition.display_count
            ) {
                last_display_count = proposition.display_count;
                proposition_group.push(proposition);
            } else {
                last_display_count = proposition.display_count;
                grouped_by_display_count_propositions.push(proposition_group);
                proposition_group = [
                    proposition,
                ];
            }
        });
        /* istanbul ignore else */
        if (proposition_group.length > 0) {
            grouped_by_display_count_propositions.push(proposition_group);
        }
        return grouped_by_display_count_propositions;
    }


    /**
     * @param {Array} grouped_proposition_list
     * @return {Array}
     */
    buildDistributedList(grouped_proposition_list) {
        const probability_list = [];

        for (let index = 0; index < grouped_proposition_list.length; index++) {
            grouped_proposition_list[index].forEach((proposition) => {
                for (let index_1 = 0; index_1 < (index + 1); index_1++) {
                    probability_list.push(proposition);
                }
            });
        }

        return probability_list;
    }

    /**
     * @param {Array} probability_list
     * @returns {Object}
     */
    performRandomChoiceFromList(probability_list) {
        const random_index = Math.ceil(Math.random() * probability_list.length) - 1;
        return probability_list[random_index];
    }
}

EloSorterService.instance = null;

module.exports = {
    EloSorterService,
};
