/* istanbul ignore file */

const {
    GalleryService,
} = require('../services');
const {
    GalleryImageRepository,
} = require('../repositories');


const gallery_service = GalleryService.getInstance();
const gallery_image_repository = GalleryImageRepository.getInstance();

(async () => {

    console.log('Loading data...');
    const gallery_list = await gallery_service.searchWithStatistics();

    for (let i = 0; i < gallery_list.length; i++) {
        const gallery_images_list = await gallery_image_repository
            .listFilesInGalleryFolder(gallery_list[i].folder);
        gallery_list[i].images_list = gallery_images_list;
    }

    console.log(`${gallery_list.length} galleries loaded`);

    console.log('comparing');
    gallery_list.forEach((gallery) => {
        console.log('processing : ', gallery.id);
        gallery_list.forEach((_gallery) => {
            if (gallery.id !== _gallery.id) {
                const duplicates = gallery.images_list
                    .filter((image) => _gallery.images_list.includes(image));
                if (duplicates.length > 0) {
                    console.log(`Found ${duplicates.length} duplicates with ${_gallery.id}`);
                }
            }
        });
    });

})();
