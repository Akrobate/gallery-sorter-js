/* istanbul ignore file */

const {
    ImageMetaDataExtractor,
} = require('../services');


const image_meta_data_extractor = ImageMetaDataExtractor.getInstance();

(async () => {
    await image_meta_data_extractor.processCreateUpdateImagesMetaData();
})();
