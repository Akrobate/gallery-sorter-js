/* istanbul ignore file */

const {
    PromptService,
} = require('../services');


const prompt_service = PromptService.getInstance();

(async () => {
    await prompt_service.generateUniquePrompt();

    await prompt_service.calculateUniqueWords();
})();
