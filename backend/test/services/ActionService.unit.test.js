'use strict';

const {
    expect,
} = require('chai');


const {
    ActionService,
} = require('../../src/services');

describe('ActionService', () => {

    const action_service = ActionService.getInstance();

    it('checkExists', () => {
        try {
            action_service.checkExists('unexisting/folder/');
        } catch (error) {
            expect(error).to.be.an('error');
        }
    });
});

