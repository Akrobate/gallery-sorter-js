'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');

const {
    ImageMetaDataExtractor,
} = require('../../src/services');

const {
    ImageMetaDataRepository,
} = require('../../src/repositories');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createGalleriesTagsJsonFile,
} = require('../test_helpers/test_helpers');

describe('PromptService unit', () => {

    const image_meta_data_repository = ImageMetaDataRepository.getInstance();
    const image_meta_data_extractor = ImageMetaDataExtractor.getInstance();
    const mocks = {};

    beforeEach(async () => {
        mocks.image_meta_data_extractor = mock(image_meta_data_extractor);
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        await createGalleriesTagsJsonFile();
    });

    afterEach(() => {
        mocks.image_meta_data_extractor.restore();
    });

    describe('parseStableDiffusionData', () => {
        it('Parsing prompt with negative prompt', () => {
            const metadata = {
                ImageWidth: 512,
                ImageHeight: 640,
                BitDepth: 8,
                ColorType: 'RGB',
                Compression: 'Deflate/Inflate',
                Filter: 'Adaptive',
                Interlace: 'Noninterlaced',
                parameters: 'beautiful house, garden, sunny day <lora:add_detail:1>\nNegative prompt: easynegative, 3d, cartoon, badhandv4, ugly, mutation, deformed, blurry, bad anatomy, out of frame, extra limbs, (text)\nSteps: 20, Sampler: DPM++ 2M, Schedule type: Karras, CFG scale: 7.0, Seed: 425699647, Face restoration: CodeFormer, Size: 512x640, Model hash: 40d4f9d626, Model: sardonyxREDUX_v20, Denoising strength: 0.4, ADetailer model: face_yolov8n.pt, ADetailer confidence: 0.3, ADetailer dilate/erode: 4, ADetailer mask blur: 4, ADetailer denoising strength: 0.4, ADetailer inpaint only masked: True, ADetailer inpaint padding: 32, ADetailer version: 23.10.1, Lora hashes: "add_detail: 7c6bad76eb54", Mask blur: 4, Inpaint area: Only masked, Masked area padding: 32, TI hashes: "easynegative: c74b4e810b03, badhandv4: 5e40d722fc3d", Version: v1.9.4',
            };

            const result = image_meta_data_extractor.parseStableDiffusionData(metadata);
            expect(result).to.be.an('object');
            expect(result).to.have.property('prompt');
            expect(result.prompt).to.equal('beautiful house, garden, sunny day <lora:add_detail:1>');

            expect(result).to.have.property('negative_prompt');
            expect(result.negative_prompt).to.equal('easynegative, 3d, cartoon, badhandv4, ugly, mutation, deformed, blurry, bad anatomy, out of frame, extra limbs, (text)');

            expect(result).to.have.property('options');
            expect(result).to.be.an('object');
            expect(result.options).to.have.property('Model');
        });


        it('Parsing prompt without Steps Option with Clip skip', () => {
            const metadata = {
                ImageWidth: 512,
                ImageHeight: 640,
                BitDepth: 8,
                ColorType: 'RGB',
                Compression: 'Deflate/Inflate',
                Filter: 'Adaptive',
                Interlace: 'Noninterlaced',
                parameters: 'beautiful house, garden, sunny day <lora:add_detail:1>\nNegative prompt: easynegative, 3d, cartoon, badhandv4, ugly, mutation, deformed, blurry, bad anatomy, out of frame, extra limbs, (text)\nClip skip: 2, Sampler: DPM++ 2M, Schedule type: Karras, CFG scale: 7.0, Seed: 425699647, Face restoration: CodeFormer, Size: 512x640, Model hash: 40d4f9d626, Model: sardonyxREDUX_v20, Denoising strength: 0.4, ADetailer model: face_yolov8n.pt, ADetailer confidence: 0.3, ADetailer dilate/erode: 4, ADetailer mask blur: 4, ADetailer denoising strength: 0.4, ADetailer inpaint only masked: True, ADetailer inpaint padding: 32, ADetailer version: 23.10.1, Lora hashes: "add_detail: 7c6bad76eb54", Mask blur: 4, Inpaint area: Only masked, Masked area padding: 32, TI hashes: "easynegative: c74b4e810b03, badhandv4: 5e40d722fc3d", Version: v1.9.4',
            };

            const result = image_meta_data_extractor.parseStableDiffusionData(metadata);
            expect(result).to.be.an('object');
            expect(result).to.have.property('prompt');
            expect(result.prompt).to.equal('beautiful house, garden, sunny day <lora:add_detail:1>');

            expect(result).to.have.property('negative_prompt');
            expect(result.negative_prompt).to.equal('easynegative, 3d, cartoon, badhandv4, ugly, mutation, deformed, blurry, bad anatomy, out of frame, extra limbs, (text)');

            expect(result).to.have.property('options');
            expect(result).to.be.an('object');
            expect(result.options).to.have.property('Model');
        });

        it('Parsing prompt without negative prompt', () => {
            const metadata = {
                ImageWidth: 512,
                ImageHeight: 640,
                BitDepth: 8,
                ColorType: 'RGB',
                Compression: 'Deflate/Inflate',
                Filter: 'Adaptive',
                Interlace: 'Noninterlaced',
                parameters: 'beautiful house, garden, sunny day <lora:add_detail:1>\nSteps: 20, Sampler: DPM++ 2M, Schedule type: Karras, CFG scale: 7.0, Seed: 425699647, Face restoration: CodeFormer, Size: 512x640, Model hash: 40d4f9d626, Model: sardonyxREDUX_v20, Denoising strength: 0.4, ADetailer model: face_yolov8n.pt, ADetailer confidence: 0.3, ADetailer dilate/erode: 4, ADetailer mask blur: 4, ADetailer denoising strength: 0.4, ADetailer inpaint only masked: True, ADetailer inpaint padding: 32, ADetailer version: 23.10.1, Lora hashes: "add_detail: 7c6bad76eb54", Mask blur: 4, Inpaint area: Only masked, Masked area padding: 32, TI hashes: "easynegative: c74b4e810b03, badhandv4: 5e40d722fc3d", Version: v1.9.4',
            };

            const result = image_meta_data_extractor.parseStableDiffusionData(metadata);
            expect(result).to.be.an('object');
            expect(result).to.have.property('prompt');
            expect(result.prompt).to.equal('beautiful house, garden, sunny day <lora:add_detail:1>');

            expect(result).to.have.property('negative_prompt');
            expect(result.negative_prompt).to.equal('');

            expect(result).to.have.property('options');
            expect(result).to.be.an('object');
            expect(result.options).to.have.property('Model');
        });


        it('parseStableDiffusionData no parameters should throw error', () => {
            const metadata = {
                ImageWidth: 512,
                ImageHeight: 640,
                BitDepth: 8,
                ColorType: 'RGB',
                Compression: 'Deflate/Inflate',
                Filter: 'Adaptive',
                Interlace: 'Noninterlaced',
            };

            try {
                image_meta_data_extractor.parseStableDiffusionData(metadata);
            } catch (error) {
                expect(error).to.be.an('error');
            }

        });

    });

    describe('processCreateUpdateImagesMetaData', () => {
        it('processCreateUpdateImagesMetaData count compare', async () => {
            const result = await image_meta_data_extractor.processCreateUpdateImagesMetaData();
            const {
                count: extracted_count,
            } = result;

            const meta_data = await image_meta_data_repository.search();
            expect(extracted_count).to.equal(meta_data.length);

            const [
                first_element,
            ] = meta_data;

            expect(first_element).to.have.property('id');
            expect(first_element).to.have.property('image_path');
            expect(first_element).to.have.property('Size');
            expect(first_element).to.have.property('Model');
            expect(first_element).to.have.property('prompt');
            expect(first_element).to.have.property('negative_prompt');
            expect(first_element).to.have.property('gallery_name');
            expect(first_element).to.have.property('gallery_id');
        });


        it('processCreateUpdateImagesMetaData count compare (executed twice)', async () => {
            const {
                count: extracted_count,
            } = await image_meta_data_extractor.processCreateUpdateImagesMetaData();

            const meta_data = await image_meta_data_repository.search();
            expect(extracted_count).to.equal(meta_data.length);

            const [
                first_element,
            ] = meta_data;

            expect(first_element).to.have.property('id');
            expect(first_element).to.have.property('image_path');
            expect(first_element).to.have.property('Size');
            expect(first_element).to.have.property('Model');
            expect(first_element).to.have.property('prompt');
            expect(first_element).to.have.property('negative_prompt');
            expect(first_element).to.have.property('gallery_name');
            expect(first_element).to.have.property('gallery_id');

            const {
                count: extracted_count_2,
            } = await image_meta_data_extractor.processCreateUpdateImagesMetaData();

            const meta_data_2 = await image_meta_data_repository.search();
            expect(extracted_count_2).to.equal(meta_data.length);

            const [
                first_element_2,
            ] = meta_data_2;

            expect(first_element_2).to.have.property('id');
            expect(first_element_2).to.have.property('image_path');
            expect(first_element_2).to.have.property('Size');
            expect(first_element_2).to.have.property('Model');
            expect(first_element_2).to.have.property('prompt');
            expect(first_element_2).to.have.property('negative_prompt');
            expect(first_element_2).to.have.property('gallery_name');
            expect(first_element_2).to.have.property('gallery_id');
        });

    });

});

