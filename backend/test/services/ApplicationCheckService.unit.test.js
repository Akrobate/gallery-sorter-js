'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createActionsJsonFile,
} = require('../test_helpers/test_helpers');
const {
    ApplicationService,
    GalleryService,
} = require('../../src/services');


describe('ApplicationService unit', () => {

    const application_service = ApplicationService.getInstance();

    const mocks = {};

    beforeEach(() => {
        mocks.application_service = mock(application_service);
    });

    afterEach(() => {
        mocks.application_service.restore();
    });


    describe('check', () => {
        it('no duplicates', async () => {
            const result = await application_service.check();
            expect(result).to.be.an('Object');
            expect(result.status).to.equal('ok');
        });

        it('with duplicates', async () => {

            mocks.application_service
                .expects('findDuplicatesId')
                .once()
                .resolves(['id_duplicate_1', 'id_duplicate_2']);

            const result = await application_service.check();
            expect(result).to.be.an('Object');
            expect(result.status).to.equal('duplicate_error');
            expect(result.message).to.equal('id_duplicate_1,id_duplicate_2');
        });
    });


    describe('findDuplicatesId', () => {
        it('no duplicates', () => {
            const result = application_service.findDuplicatesId([
                'aaa', 'bbb', 'ccc',
            ]);

            expect(result).to.be.an('Array');
            expect(result.length).to.equal(0);
        });

        it('One duplicate', () => {
            const result = application_service.findDuplicatesId([
                'aaa', 'bbb', 'ccc', 'aaa',
            ]);

            expect(result).to.be.an('Array');
            expect(result.length).to.equal(1);
            const [
                first,
            ] = result;
            expect(first).to.equal('aaa');
        });

        it('Two duplicates', () => {
            const result = application_service.findDuplicatesId([
                'aaa', 'bbb', 'ccc', 'aaa', 'zzz', 'uuu', 'zzz',
            ]);

            expect(result).to.be.an('Array');
            expect(result.length).to.equal(2);
            const [
                first,
                second,
            ] = result;
            expect(first).to.equal('aaa');
            expect(second).to.equal('zzz');
        });
    });


    describe('findGalleryActionHotKeysCollision', () => {

        beforeEach(async () => {
            mocks.gallery_serivce = mock(GalleryService.getInstance());
            await removeTestApplicationFolder();
            await createGalleriesJsonFile();
            await createActionsJsonFile();
        });

        afterEach(() => {
            mocks.gallery_serivce.restore();
        });

        it('no duplicates', async () => {
            const result = await application_service.findGalleryActionHotKeysCollision();
            expect(result).to.equal(false);
        });

        it('Action duplicated', async () => {
            mocks.gallery_serivce
                .expects('search')
                .once()
                .resolves(
                    [
                        {
                            actions: [
                                {
                                    hotkey: 'r',
                                },
                                {
                                    hotkey: 'z',
                                },
                            ],
                        },
                    ]
                );
            const result = await application_service.findGalleryActionHotKeysCollision();
            expect(result).to.equal(true);
        });


    });
});
