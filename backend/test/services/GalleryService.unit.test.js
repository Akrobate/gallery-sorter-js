'use strict';

const {
    expect,
} = require('chai');


const {
    GalleryService,
} = require('../../src/services');

describe('GalleryService', () => {

    const gallery_service = GalleryService.getInstance();

    it('enrichGalleryWithActions without actions defined', async () => {
        const gallery = {
            id: 1,
        };
        const response = await gallery_service.enrichGalleryWithActions(gallery);
        expect(response).to.deep.equal(gallery);
    });
});

