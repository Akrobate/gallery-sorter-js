'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    refreshTestGalleriesDataFolder,
} = require('../test_helpers/test_helpers');
const {
    PromptService,
    ImageMetaDataExtractor,
} = require('../../src/services');
const {
    PromptRepository,
} = require('../../src/repositories');

describe('PromptService unit', () => {

    const prompt_service = PromptService.getInstance();
    const prompt_repositories = PromptRepository.getInstance();
    const image_meta_data_extractor = ImageMetaDataExtractor.getInstance();
    const mocks = {};

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        refreshTestGalleriesDataFolder();

        mocks.prompt_service = mock(prompt_service);
        mocks.prompt_repositories = mock(prompt_repositories);
    });


    afterEach(() => {
        mocks.prompt_service.restore();
        mocks.prompt_repositories.restore();

    });


    describe('normalizePromptToWords', () => {
        it('Parsing prompt', () => {
            const prompt = '(Test), Other, <lora:coucou>,(with poderation:10)';
            const result = prompt_service.normalizePromptToWords(prompt);
            expect(result).to.be.an('array');
            expect(result).to.have.length(4);
            expect(result).to.deep.equal([
                'Test',
                'Other',
                'lora:coucou',
                'with poderation',
            ]);
        });
    });

    describe('calculateUniquePrompts', () => {
        it('calculateUniquePrompts', async () => {
            await image_meta_data_extractor.processCreateUpdateImagesMetaData();

            const result = await prompt_service.calculateUniquePrompts();

            expect(result).to.be.an('array');
            expect(result.length).to.be.greaterThan(0);

            const [
                first_result,
            ] = result;

            expect(first_result).to.be.an('object');
            expect(first_result).to.have.property('prompt');
            expect(first_result).to.have.property('negative_prompt');
            expect(first_result).to.have.property('gallery_id_list');

            const [
                first_gallery_id,
            ] = first_result.gallery_id_list;

            expect(first_gallery_id).to.be.an('object');
            expect(first_gallery_id).to.have.property('gallery_id');
            expect(first_gallery_id).to.have.property('Clip skip');
            expect(first_gallery_id).to.have.property('Model');

        });

        it('calculateUniqueWords', async () => {

            await image_meta_data_extractor.processCreateUpdateImagesMetaData();
            await prompt_service.generateUniquePrompt();
            const result = await prompt_service.calculateUniqueWords();
            expect(result).to.be.an('array');

            expect(result.length).to.be.greaterThan(1);

            const [
                word,
            ] = result;

            expect(word).to.be.an('object');
            expect(word).to.have.property('word');
            expect(word).to.have.property('count');


            const second_result = await prompt_service.calculateUniqueWords();
            expect(second_result).to.deep.equal(result);

        });


        it('calculateUniqueWords mocked data', async () => {

            mocks.prompt_repositories
                .expects('search')
                .once()
                .resolves([
                    {
                        prompt: 'Test, Other',
                        gallery_id_list: [{}],
                    },
                    {
                        prompt: 'Test, Other',
                        gallery_id_list: [{}],
                    },
                    {
                        prompt: 'Test, Other',
                        gallery_id_list: [{}],
                    },
                ]);

            const result = await prompt_service.calculateUniqueWords();
            expect(result).to.be.an('array');
            expect(result).to.deep.equal([
                {
                    word: 'Test',
                    count: 3,
                },
                {
                    word: 'Other',
                    count: 3,
                },
            ]);
        });
    });


    describe('generateUniquePrompt', () => {

        it('generateUniquePrompt', async () => {

            await image_meta_data_extractor.processCreateUpdateImagesMetaData();

            const results_before = await prompt_service.prompt_repository.search({});

            expect(results_before).to.be.an('array');
            expect(results_before.length).to.equal(0);

            await prompt_service.generateUniquePrompt();

            // Called twice to check if it will not create duplicates
            await prompt_service.generateUniquePrompt();

            const results = await prompt_service.prompt_repository.search({});
            expect(results).to.be.an('array');
            expect(results.length).to.be.greaterThan(0);
            const [
                first_result,
            ] = results;
            expect(first_result).to.be.an('object');
            expect(first_result).to.have.property('prompt');
            expect(first_result).to.have.property('negative_prompt');
            expect(first_result).to.have.property('gallery_id_list');
            expect(first_result).to.have.property('id');

            const {
                gallery_id_list,
            } = first_result;

            expect(gallery_id_list).to.be.an('array');
            expect(gallery_id_list.length).to.be.greaterThan(0);

            const [
                first_gallery_id,
            ] = gallery_id_list;

            expect(first_gallery_id).to.be.an('object');
            expect(first_gallery_id).to.have.property('gallery_id');
            expect(first_gallery_id).to.have.property('Clip skip');
            expect(first_gallery_id).to.have.property('Model');

        });

    });

});
