'use strict';

const {
    expect,
} = require('chai');


const {
    EloSorterService,
} = require('../../src/services');


describe('EloSorterService', () => {

    const elo_sorter_service = EloSorterService.getInstance();

    it('groupArrayByDisplayCount', () => {
        const params = [
            {
                display_count: 1,
                id: 1,
            },
            {
                display_count: 2,
                id: 2,
            },
            {
                display_count: 1,
                id: 3,
            },
        ];
        const result = elo_sorter_service.groupArrayByDisplayCount(params);
        expect(result).to.deep.equal([
            [
                {
                    display_count: 2,
                    id: 2,
                },
            ],
            [
                {
                    display_count: 1,
                    id: 1,
                },
                {
                    display_count: 1,
                    id: 3,
                },
            ],
        ]);
    });


    it('performRandomChoiceFromList', () => {
        const params = [
            {
                display_count: 1,
                id: 1,
            },
            {
                display_count: 2,
                id: 2,
            },
            {
                display_count: 1,
                id: 3,
            },
        ];
        const result = elo_sorter_service.performRandomChoiceFromList(params);
        expect(result).to.be.an('object');
        expect(result).to.have.property('id');
        expect(result).to.have.property('display_count');
    });


    it('buildDistributedList Prototype! needs to be reviewed', () => {
        const params = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];
        const result = elo_sorter_service.buildDistributedList(params);
        expect(result).to.deep.equal([
            1, 2, 3, 4, 4, 5, 5, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9,
        ]);
    });
});
