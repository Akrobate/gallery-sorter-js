'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');

const {
    GalleryTagService,
} = require('../../src/services');


describe('GalleryTagService unit', () => {

    const gallery_tag_service = GalleryTagService.getInstance();
    const mocks = {};

    const test_tag_list = ['4:5', 'clip 1', 'v40', 'clear'];

    const formatTagList = (tag_list) => tag_list.map((name) => ({
        name,
    }));

    beforeEach(() => {
        mocks.gallery_tag_service = mock(gallery_tag_service);
    });


    afterEach(() => {
        mocks.gallery_tag_service.restore();
    });

    describe('suggestTagsFromPath', () => {
        it('case ratio_4_5/dark_style_test_lorem_ipsum_something_clip_1_v40/', async () => {
            mocks.gallery_tag_service.expects('search')
                .once()
                .resolves(formatTagList(test_tag_list));
            const name_list = await gallery_tag_service.suggestTagsFromPath(
                'ratio_4_5/dark_style_test_lorem_ipsum_something_clip_1_v40/'
            );
            expect(name_list)
                .to.include('4:5')
                .and.to.include('clip 1')
                .and.to.include('v40');
        });

        it('case ratio_4_5/clear/clear_style_style_test_lorem_ipsum_clip_1_v40/', async () => {

            mocks.gallery_tag_service.expects('search')
                .once()
                .resolves(formatTagList(test_tag_list));
            const name_list = await gallery_tag_service.suggestTagsFromPath(
                'ratio_4_5/clear/clear_style_style_test_lorem_ipsum_clip_1_v40/'
            );
            expect(name_list)
                .to.include('4:5')
                .and.to.include('clip 1')
                .and.to.include('clear')
                .and.to.include('v40');

        });

    });

});
