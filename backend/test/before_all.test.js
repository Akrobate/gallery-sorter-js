'use strict';

const {
    refreshTestGalleriesDataFolder,
} = require('./test_helpers/test_helpers');

before((done) => {
    refreshTestGalleriesDataFolder();
    done();
});
