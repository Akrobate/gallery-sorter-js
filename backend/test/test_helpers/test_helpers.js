'use strict';

const fs = require('fs');
const {
    configuration,
} = require('../../src/configuration');
const {
    JsonFileRepository,
} = require('../../src/repositories/JsonFileRepository');

const json_file_repository = JsonFileRepository.getInstance();

function removeTestApplicationFolder() {
    fs.rmSync(
        `${configuration.storage.files.application_files}/.application/`,
        {
            recursive: true,
            force: true,
        }
    );
}

async function createActionsJsonFile() {
    await json_file_repository.checkAndInitApplicationFilePath();
    await json_file_repository.create('actions.json',
        {
            id: 'REMOVE_ID',
            name: 'Remove',
            icon: 'mdi-delete-outline',
            action: 'mv',
            folder: 'trash',
        },
        false
    );
}


async function createGalleriesJsonFile() {
    await json_file_repository.checkAndInitApplicationFilePath();
    await json_file_repository.create('galleries.json',
        {
            id: 'MY_GALLERY_ID',
            name: 'Test Gallerie 1',
            folder: 'gallery_1/',
            actions: [
                {
                    id: 'REMOVE_ID',
                    name: 'Remove',
                    icon: 'mdi-delete-outline',
                    action: 'mv',
                    folder: 'trash',
                },
                {
                    id: 'OFF_TOPIC_ID',
                    name: 'Hors sujet',
                    icon: 'mdi-file-question-outline',
                    action: 'mv',
                    folder: 'off-topic',
                },
                {
                    id: 'NSFW_ID',
                    name: 'NSFW',
                    icon: 'mdi-alert-outline',
                    action: 'mv',
                    folder: 'nsfw',
                },
                {
                    id: 'SELECT_ID',
                    name: 'SELECT',
                    icon: 'mdi-alert-outline',
                    action: 'cp,mv',
                    folder: 'selected',
                    folder2: 'selected2',
                },
            ],
        },
        false
    );
    await json_file_repository.create('galleries.json',
        {
            'id': 'g1id_with_two_tags',
            'name': 'Gallery 2 (test)',
            'folder': 'gallery_2/',
            'tags': ['4:5', '2:3'],
            'actions': [
                {
                    'id': 'REMOVE_ID',
                    'name': 'Remove',
                    'icon': 'mdi-delete-outline',
                    'action': 'mv',
                    'folder': 'trash',
                },
                {
                    'id': 'OFF_TOPIC_ID',
                    'name': 'Hors sujet',
                    'icon': 'mdi-file-question-outline',
                    'action': 'mv',
                    'folder': 'off-topic',
                },
                {
                    'id': 'NSFW_ID',
                    'name': 'NSFW',
                    'icon': 'mdi-alert-outline',
                    'action': 'mv',
                    'folder': 'nsfw',
                },
            ],
        },
        false
    );
    await json_file_repository.create('galleries.json',
        {
            'id': 'g1id_with_3_tag',
            'name': 'Gallery 3 (test)',
            'folder': 'gallery_3/',
            'tags': ['4:5'],
            'actions': [
                {
                    'id': 'REMOVE_ID',
                    'name': 'Remove',
                    'icon': 'mdi-delete-outline',
                    'action': 'mv',
                    'folder': 'trash',
                },
                {
                    'id': 'OFF_TOPIC_ID',
                    'name': 'Hors sujet',
                    'icon': 'mdi-file-question-outline',
                    'action': 'mv',
                    'folder': 'off-topic',
                },
                {
                    'id': 'NSFW_ID',
                    'name': 'NSFW',
                    'icon': 'mdi-alert-outline',
                    'action': 'mv',
                    'folder': 'nsfw',
                },
            ],
        },
        false
    );

    await json_file_repository.create('galleries.json',
        {
            'id': 'g1id_with_3',
            'name': 'Gallery 3 (test)',
            'folder': 'gallery_3/',
            'tags': ['4:5'],
            'archived': false,
            'actions': [
                {
                    'id': 'REMOVE_ID',
                    'name': 'Remove',
                    'icon': 'mdi-delete-outline',
                    'action': 'mv',
                    'folder': 'trash',
                },
                {
                    'id': 'OFF_TOPIC_ID',
                    'name': 'Hors sujet',
                    'icon': 'mdi-file-question-outline',
                    'action': 'mv',
                    'folder': 'off-topic',
                },
                {
                    'id': 'NSFW_ID',
                    'name': 'NSFW',
                    'icon': 'mdi-alert-outline',
                    'action': 'mv',
                    'folder': 'nsfw',
                },
            ],
        },
        false
    );

    await json_file_repository.create('galleries.json',
        {
            'id': 'g1id_with_4_tag_archived',
            'name': 'Gallery 3 (test)',
            'folder': 'gallery_3/',
            'tags': ['4:5'],
            'archived': true,
            'actions': [
                {
                    'id': 'REMOVE_ID',
                    'name': 'Remove',
                    'icon': 'mdi-delete-outline',
                    'action': 'mv',
                    'folder': 'trash',
                },
                {
                    'id': 'OFF_TOPIC_ID',
                    'name': 'Hors sujet',
                    'icon': 'mdi-file-question-outline',
                    'action': 'mv',
                    'folder': 'off-topic',
                },
                {
                    'id': 'NSFW_ID',
                    'name': 'NSFW',
                    'icon': 'mdi-alert-outline',
                    'action': 'mv',
                    'folder': 'nsfw',
                },
            ],
        },
        false
    );
}


async function createGalleriesTagsJsonFile() {
    await json_file_repository.checkAndInitApplicationFilePath();
    await json_file_repository.create('galleries_tags.json',
        {
            name: '4:5',
            color: '#ff0000',
            category: 'ratio',
        },
        false
    );
}


async function createEloScoreMyGalleryIncompleteWithRemoves() {
    await json_file_repository.checkAndInitApplicationFilePath();

    // Existing image
    await json_file_repository.create('elo_score_MY_GALLERY_ID.json',
        {
            filename: '1726063288842.png',
            elo_score: 1000,
            display_count: 0,
            created_timestamp: 1731769602,
            updated_timestamp: 1731769602,
            id: '254af074-ef62-436e-95b0-9428b8189dc2',
        },
        false
    );

    // Not existing image
    await json_file_repository.create('elo_score_MY_GALLERY_ID.json',
        {
            filename: '1826063298842.png',
            elo_score: 1000,
            display_count: 0,
            created_timestamp: 1731769602,
            updated_timestamp: 1731769602,
            id: '254af074-ef62-436e-95b0-9428b8189dc3',
        },
        false
    );
}


function removeDataGalleriesFolder() {
    fs.rmSync(
        `${configuration.storage.files.galleries_files_path}`,
        {
            recursive: true,
            force: true,
        }
    );
}


function copySeedGalleriesFolderToDataFolder() {
    fs.cpSync(
        './test/seeds/galleries/',
        `${configuration.storage.files.galleries_files_path}`,
        {
            recursive: true,
        }
    );
}

function refreshTestGalleriesDataFolder() {
    removeDataGalleriesFolder();
    copySeedGalleriesFolderToDataFolder();
}


module.exports = {
    removeTestApplicationFolder,
    createActionsJsonFile,
    createGalleriesJsonFile,
    createGalleriesTagsJsonFile,
    createEloScoreMyGalleryIncompleteWithRemoves,
    removeDataGalleriesFolder,
    copySeedGalleriesFolderToDataFolder,
    refreshTestGalleriesDataFolder,
};

