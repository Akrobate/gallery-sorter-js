'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createActionsJsonFile,
    refreshTestGalleriesDataFolder,
} = require('../test_helpers/test_helpers');
const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Galleries actions', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        await createActionsJsonFile();
        refreshTestGalleriesDataFolder();
    });

    afterEach(() => {
        refreshTestGalleriesDataFolder();
    });

    it('REMOVE_ID', async () => {

        const gallery_id = 'MY_GALLERY_ID';
        const action_id = 'REMOVE_ID';
        const filename = '1726063288842.png';

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/process-action/${action_id}`)
            .send({
                filename,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('Action processed');
            });
    });

    it('OFF_TOPIC_ID', async () => {

        const gallery_id = 'MY_GALLERY_ID';
        const action_id = 'OFF_TOPIC_ID';
        const filename = '1726063288842.png';

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/process-action/${action_id}`)
            .send({
                filename,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('Action processed');
            });
    });


    it('NSFW_ID', async () => {

        const gallery_id = 'MY_GALLERY_ID';
        const action_id = 'NSFW_ID';
        const filename = '1726063288842.png';

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/process-action/${action_id}`)
            .send({
                filename,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('Action processed');
            });
    });

    it('SELECT_ID', async () => {
        const gallery_id = 'MY_GALLERY_ID';
        const action_id = 'SELECT_ID';
        const filename = '1726063288842.png';

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/process-action/${action_id}`)
            .send({
                filename,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('Action processed');
            });
    });
});

