'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    mock,
} = require('sinon');
const {
    expect,
} = require('chai');
const {
    GalleryRepository,
} = require('../../src/repositories');
const {
    app,
} = require('../../src/app');

const gallery_repository = GalleryRepository.getInstance();

const superApp = superTest(app);

const mocks = {};

describe('Gallery images', () => {

    beforeEach(() => {
        mocks.gallery_repository = mock(gallery_repository);
    });

    afterEach(() => {
        mocks.gallery_repository.restore();
    });

    it('Get galleries images', async () => {
        mocks.gallery_repository
            .expects('get')
            .resolves({
                id: 'MY_GALLERY_ID',
                name: 'Test Gallerie 1',
                folder: 'gallery_1/',
            });

        await superApp
            .get('/api/v1/galleries/MY_GALLERY_ID/images')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('image_list');
                expect(response.body.image_list).to.be.an('array');
                expect(response.body.image_list.length).to.equal(3);
            });
    });
});
