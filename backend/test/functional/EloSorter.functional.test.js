'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createEloScoreMyGalleryIncompleteWithRemoves,
} = require('../test_helpers/test_helpers');

const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');

const superApp = superTest(app);

describe('EloSorter functional', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        await createEloScoreMyGalleryIncompleteWithRemoves();
    });

    it('init', async () => {
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .post(`/api/v1/galleries/${gallery_id}/init-elo`)
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
            });
    });


    it('getEloRanking', async () => {
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .get(`/api/v1/galleries/${gallery_id}/elo-results`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
            });
    });


    it('getRandom', async () => {
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-get-random`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('Object');
                expect(response.body).to.have.property('filename');
                expect(response.body).to.have.property('elo_score');
                expect(response.body).to.have.property('display_count');
                expect(response.body).to.have.property('created_timestamp');
                expect(response.body).to.have.property('updated_timestamp');
                expect(response.body).to.have.property('id');
                expect(response.body).to.have.property('url');
            });
    });

    it('getRandom priority_lower_display_count true', async () => {
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-get-random`)
            .send({
                priority_lower_display_count: true,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('Object');
                expect(response.body).to.have.property('filename');
                expect(response.body).to.have.property('elo_score');
                expect(response.body).to.have.property('display_count');
                expect(response.body).to.have.property('created_timestamp');
                expect(response.body).to.have.property('updated_timestamp');
                expect(response.body).to.have.property('id');
                expect(response.body).to.have.property('url');
            });
    });


    it('getRandom exclude list', async () => {

        let elo_score_list = [];
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .get(`/api/v1/galleries/${gallery_id}/elo-results`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
                // eslint-disable-next-line prefer-destructuring
                elo_score_list = response.body.elo_score_list;
            });

        const [
            first_element,
            second_element,
        ] = elo_score_list;

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-get-random`)
            .send({
                exclude_id_list: [first_element.id],
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('Object');
                expect(response.body).to.have.property('id', second_element.id);
            });
    });


    it('processEloDuelResult Nobody wins', async () => {
        const gallery_id = 'MY_GALLERY_ID';

        let player_1 = {};
        let player_2 = {};
        await superApp
            .get(`/api/v1/galleries/${gallery_id}/elo-results`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
                ([
                    player_1,
                    player_2,
                ] = response.body.elo_score_list);
            });
        expect(player_1).to.have.property('elo_score', 1000);
        expect(player_2).to.have.property('elo_score', 1000);

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-duel-result`)
            .send({
                elo_score_id_1: player_1.id,
                elo_score_id_2: player_2.id,
                winner: 0,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_result_list');

                const _player_1 = response.body.elo_result_list
                    .find((player) => player.id === player_1.id);
                const _player_2 = response.body.elo_result_list
                    .find((player) => player.id === player_2.id);

                expect(_player_1).to.have.property('elo_score', 1000);
                expect(_player_2).to.have.property('elo_score', 1000);

            });
    });


    it('processEloDuelResult player 1 wins', async () => {
        const gallery_id = 'MY_GALLERY_ID';

        let player_1 = {};
        let player_2 = {};
        await superApp
            .get(`/api/v1/galleries/${gallery_id}/elo-results`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
                ([
                    player_1,
                    player_2,
                ] = response.body.elo_score_list);
            });
        expect(player_1).to.have.property('elo_score', 1000);
        expect(player_2).to.have.property('elo_score', 1000);

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-duel-result`)
            .send({
                elo_score_id_1: player_1.id,
                elo_score_id_2: player_2.id,
                winner: 1,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_result_list');

                const _player_1 = response.body.elo_result_list
                    .find((player) => player.id === player_1.id);
                const _player_2 = response.body.elo_result_list
                    .find((player) => player.id === player_2.id);

                expect(_player_1).to.have.property('elo_score', 1020);
                expect(_player_2).to.have.property('elo_score', 980);

            });
    });


    it('processEloDuelResult player 2 wins', async () => {
        const gallery_id = 'MY_GALLERY_ID';

        let player_1 = {};
        let player_2 = {};
        await superApp
            .get(`/api/v1/galleries/${gallery_id}/elo-results`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_score_list');
                ([
                    player_1,
                    player_2,
                ] = response.body.elo_score_list);
            });
        expect(player_1).to.have.property('elo_score', 1000);
        expect(player_2).to.have.property('elo_score', 1000);

        await superApp
            .post(`/api/v1/galleries/${gallery_id}/elo-duel-result`)
            .send({
                elo_score_id_1: player_1.id,
                elo_score_id_2: player_2.id,
                winner: 2,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('elo_result_list');

                const _player_1 = response.body.elo_result_list
                    .find((player) => player.id === player_1.id);
                const _player_2 = response.body.elo_result_list
                    .find((player) => player.id === player_2.id);

                expect(_player_1).to.have.property('elo_score', 980);
                expect(_player_2).to.have.property('elo_score', 1020);

            });
    });


});
