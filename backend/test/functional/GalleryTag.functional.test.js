'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createGalleriesTagsJsonFile,
} = require('../test_helpers/test_helpers');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Galleries', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        await createGalleriesTagsJsonFile();
    });

    it('Galleries Tags search', async () => {
        await superApp
            .get('/api/v1/galleries-tags')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('gallery_tag_list');
                expect(response.body.gallery_tag_list).to.be.an('array');
                expect(response.body.gallery_tag_list.length).to.be.eq(2);
                expect(response.body.gallery_tag_list.map((item) => item.name)).to.include('2:3');
                expect(response.body.gallery_tag_list.map((item) => item.name)).to.include('4:5');
            });
    });

});

