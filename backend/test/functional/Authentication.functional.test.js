'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    stub,
} = require('sinon');

const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
} = require('../test_helpers/test_helpers');

const {
    app,
    authentication_middleware,
} = require('../../src/app');


const superApp = superTest(app);

describe('Authentication.functional.test', () => {

    before(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
    });

    it('No configuration token authorization', async () => {
        const token = null;
        const authentication_stub = stub(authentication_middleware, 'authentication_token')
            .get(() => token);
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .get(`/api/v1/galleries/${gallery_id}`)
            .expect(HTTP_CODE.OK);
        authentication_stub.restore();
    });

    it('Configuration token authorization without authorization', async () => {
        const token = 'MY_TOKEN_';
        const authentication_stub = stub(authentication_middleware, 'authentication_token')
            .get(() => token);
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .get(`/api/v1/galleries/${gallery_id}`)
            .expect(HTTP_CODE.UNAUTHORIZED);
        authentication_stub.restore();
    });

    it('Configuration token authorization whith authorization', async () => {
        const token = 'MY_TOKEN_';
        const authentication_stub = stub(authentication_middleware, 'authentication_token')
            .get(() => token);
        const gallery_id = 'MY_GALLERY_ID';
        await superApp
            .get(`/api/v1/galleries/${gallery_id}`)
            .set('Authorization', `${token}`)
            .expect(HTTP_CODE.OK);
        authentication_stub.restore();
    });

});
