'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
} = require('../test_helpers/test_helpers');

const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Bookmarks', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
    });

    it('Create bookmark', async () => {
        await superApp
            .post('/api/v1/bookmarks')
            .send({
                filename: 'test',
                gallery_id: 'MY_GALLERY_ID',
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('id');
            });
    });


    it('Should not be able to create bookmark if gallery is unknown', async () => {
        await superApp
            .post('/api/v1/bookmarks')
            .send({
                filename: 'test',
                gallery_id: 'MY_GALLERY_ID_UNEXISTING',
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('id');
            });

        await superApp
            .get('/api/v1/bookmarks')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('bookmark_list');
                expect(response.body.bookmark_list.length).to.equal(0);
            });
    });


    it('Should not be able to create bookmark if bad params', async () => {
        await superApp
            .post('/api/v1/bookmarks')
            .send({
                filename: 'test',
            })
            .expect(HTTP_CODE.BAD_REQUEST)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message');
                expect(response.body.message).to.equal('"body.gallery_id" is required');
            });
    });

    it('Search bookmark', async () => {
        await superApp
            .post('/api/v1/bookmarks')
            .send({
                filename: 'test',
                gallery_id: 'MY_GALLERY_ID',
            })
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/bookmarks')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('bookmark_list');
                const [
                    first_element,
                ] = response.body.bookmark_list;
                expect(first_element).to.be.an('object');
                expect(first_element).to.have.property('filename', 'test');
                expect(first_element).to.have.property('gallery_id', 'MY_GALLERY_ID');
                expect(first_element).to.have.property('gallery');
            });
    });

    it('delete bookmark', async () => {
        await superApp
            .post('/api/v1/bookmarks')
            .send({
                filename: 'test',
                gallery_id: 'MY_GALLERY_ID',
            })
            .expect(HTTP_CODE.OK);

        let first_element = null;
        await superApp
            .get('/api/v1/bookmarks')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('bookmark_list');
                [
                    first_element,
                ] = response.body.bookmark_list;
                expect(first_element).to.be.an('object');
                expect(first_element).to.have.property('filename', 'test');
                expect(first_element).to.have.property('gallery_id', 'MY_GALLERY_ID');
                expect(first_element).to.have.property('gallery');
            });

        await superApp
            .delete(`/api/v1/bookmarks/${first_element.id}`)
            .expect(HTTP_CODE.OK)
            .expect(
                ({
                    body,
                }) => {

                    expect(body).to.be.an('object');
                    expect(body).to.have.property('deleted', true);

                }
            );

        await superApp
            .get('/api/v1/bookmarks')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('bookmark_list');
                expect(response.body.bookmark_list.length).to.equal(0);
            });
    });

});

