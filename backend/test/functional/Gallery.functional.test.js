'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    createActionsJsonFile,
} = require('../test_helpers/test_helpers');
const {
    expect,
} = require('chai');
const {
    stringify,
} = require('qs');
const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Galleries', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        await createActionsJsonFile();
    });

    it('Get galleries', async () => {
        await superApp
            .get('/api/v1/galleries/MY_GALLERY_ID')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('id', 'MY_GALLERY_ID');
            });
    });


    it('Search galleries', async () => {
        await superApp
            .get('/api/v1/galleries')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('gallery_list');
                expect(response.body.gallery_list).to.be.an('array');
            });
    });

    it('Search galleries with tags', async () => {
        await superApp
            .get('/api/v1/galleries')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.have.property('gallery_list');
                expect(response.body.gallery_list).to.be.an('array');
                const [
                    first_result_without_tags,
                    second_result_with_tags,
                ] = response.body.gallery_list;

                expect(first_result_without_tags).to.have.property('tags');
                expect(first_result_without_tags.tags).to.be.an('array');

                expect(second_result_with_tags).to.have.property('tags');
                expect(second_result_with_tags.tags).to.be.an('array');
                expect(second_result_with_tags.tags.length).to.be.gt(1);
            });
    });


    describe('Gallery search tags', () => {
        it('Empty array tags', async () => {
            await superApp
                .get('/api/v1/galleries')
                .query(stringify({
                    tag_list: [],
                }))
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.gte(3);
                });
        });

        it('One tag', async () => {
            await superApp
                .get('/api/v1/galleries')
                .query(stringify({
                    tag_list: ['4:5'],
                }))
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.eq(4);
                    const [
                        result,
                    ] = response.body.gallery_list;
                    expect(result.tags).to.be.an('array');
                    expect(result.tags).to.include('4:5');
                });
        });

        it('Two tags', async () => {
            await superApp
                .get('/api/v1/galleries')
                .query(stringify({
                    tag_list: ['2:3', '4:5'],
                }))
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.eq(1);
                    const [
                        result,
                    ] = response.body.gallery_list;
                    expect(result.tags).to.be.an('array');
                    expect(result.tags).to.include('4:5');
                    expect(result.tags).to.include('2:3');
                });
        });
    });


    describe('Gallery search archived', () => {
        it('Empty array tags', async () => {
            await superApp
                .get('/api/v1/galleries')
                .query(stringify({
                    archived: true,
                }))
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.gte(1);
                    const [
                        first_result,
                    ] = response.body.gallery_list;

                    expect(first_result).to.have.property('archived', true);

                });
        });
    });


    describe('Gallery with statistics', () => {
        it('Search without params', async () => {
            await superApp
                .get('/api/v1/galleries-with-statistics')
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.gte(3);

                    const {
                        gallery_list,
                    } = response.body;

                    expect(gallery_list).to.be.an('array');
                    expect(gallery_list.length).to.be.gte(1);
                    const [
                        gallery,
                    ] = gallery_list;

                    expect(gallery).to.have.property('elo_scores_count');
                    expect(gallery).to.have.property('total_played');
                    expect(gallery).to.have.property('average_played');
                    expect(gallery).to.have.property('never_played_count');
                    expect(gallery).to.have.property('best_image');
                    expect(gallery).to.have.property('actions');

                    const {
                        actions,
                    } = gallery;

                    expect(actions).to.be.an('array');
                    expect(actions.length).to.be.gte(1);

                    const [
                        action,
                    ] = actions;
                    expect(action).to.have.property('folder_file_count');
                });
        });


        it('Search without params with played elo', async () => {

            const gallery_id = 'MY_GALLERY_ID';
            let all_images = [];

            await superApp
                .post(`/api/v1/galleries/${gallery_id}/init-elo`)
                .expect(HTTP_CODE.CREATED)
                .expect((response) => {
                    all_images = response.body.elo_score_list;
                });

            await superApp
                .post(`/api/v1/galleries/${gallery_id}/elo-duel-result`)
                .send({
                    elo_score_id_1: all_images[0].id,
                    elo_score_id_2: all_images[1].id,
                    winner: 1,
                })
                .expect(HTTP_CODE.OK);

            await superApp
                .post(`/api/v1/galleries/${gallery_id}/elo-duel-result`)
                .send({
                    elo_score_id_1: all_images[1].id,
                    elo_score_id_2: all_images[2].id,
                    winner: 1,
                })
                .expect(HTTP_CODE.OK);

            await superApp
                .get('/api/v1/galleries-with-statistics')
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.have.property('gallery_list');
                    expect(response.body.gallery_list).to.be.an('array');
                    expect(response.body.gallery_list.length).to.be.gte(3);

                    const {
                        gallery_list,
                    } = response.body;

                    expect(gallery_list).to.be.an('array');
                    expect(gallery_list.length).to.be.gte(1);
                    const [
                        gallery,
                    ] = gallery_list;

                    expect(gallery).to.have.property('elo_scores_count');
                    expect(gallery).to.have.property('total_played');
                    expect(gallery).to.have.property('average_played');
                    expect(gallery).to.have.property('never_played_count');
                    expect(gallery).to.have.property('best_image');
                    expect(gallery).to.have.property('actions');

                    const {
                        actions,
                    } = gallery;

                    expect(actions).to.be.an('array');
                    expect(actions.length).to.be.gte(1);

                    const [
                        action,
                    ] = actions;
                    expect(action).to.have.property('folder_file_count');
                });
        });
    });

});

