'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
    refreshTestGalleriesDataFolder,
} = require('../test_helpers/test_helpers');
const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Prompt', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
        refreshTestGalleriesDataFolder();
    });

    afterEach(() => {
        refreshTestGalleriesDataFolder();
    });

    it('Search', async () => {

        await superApp
            .get('/api/v1/prompts')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
            });
    });

});
