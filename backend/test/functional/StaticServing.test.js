'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Static serving test', () => {
    it('Should return 200 for existing file', async () => {
        await superApp
            .get('/static/gallery_1/1726063288842.png')
            .expect(HTTP_CODE.OK);
    });

    it('Should return 404 for not existing file', async () => {
        await superApp
            .get('/static/gallery_1/NOT_EXISTING_FILE.png')
            .expect(HTTP_CODE.NOT_FOUND);
    });
});
