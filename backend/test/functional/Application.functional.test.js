'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');

const superApp = superTest(app);

describe('Application', () => {
    it('application find unknown galleries', async () => {
        await superApp
            .get('/api/v1/application/find-unknown-galleries')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('array');
            });
    });


    it('application refrech caches', async () => {
        await superApp
            .get('/api/v1/application/refresh-caches')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.a.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('ok');
            });
    });


    it('application check', async () => {
        await superApp
            .get('/api/v1/application/check')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.a.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body.status).to.equal('ok');
            });
    });

});
