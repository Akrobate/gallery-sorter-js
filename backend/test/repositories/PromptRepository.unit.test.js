
const {
    expect,
} = require('chai');

const {
    PromptRepository,
} = require('../../src/repositories');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
} = require('../test_helpers/test_helpers');

describe('PromptRepository unit', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
    });

    it('search, create, update', async () => {
        const prompt_repository = PromptRepository.getInstance();
        const results = await prompt_repository.search();
        expect(results).to.be.an('array').and.to.have.length(0);

        const result = await prompt_repository.create({
            prompt: 'Test, Other',
            gallery_id_list: [{}],
        });
        expect(result).to.be.an('object');
        const results_1 = await prompt_repository.search();
        expect(results_1).to.be.an('array').and.to.have.length(1);

        const result_2 = await prompt_repository.update(
            results_1[0].id,
            {
                prompt: 'Test, Other UPDATED',
                gallery_id_list: [{}],
            }
        );
        expect(result_2).to.be.an('object');
        const results_3 = await prompt_repository.search();
        expect(results_3).to.be.an('array').and.to.have.length(1);
        expect(results_3[0]).to.have.property('prompt', 'Test, Other UPDATED');
    });

});

