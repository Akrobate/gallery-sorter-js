
const {
    expect,
} = require('chai');

const {
    ActionRepository,
} = require('../../src/repositories');
const {
    createActionsJsonFile,
    removeTestApplicationFolder,
} = require('../test_helpers/test_helpers');

describe('ActionRepository unit', () => {

    const action_repository = ActionRepository.getInstance();

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createActionsJsonFile();
        await action_repository.loadData(true);
    });


    it('getActions', async () => {
        const result = await action_repository.search();
        expect(result).to.be.an('array');
        expect(result.length).to.equal(1);
    });


    it('enrichAction', async () => {
        const result = await action_repository.enrichAction({
            id: 'REMOVE_ID',
        });
        expect(result).to.deep.equal({
            id: 'REMOVE_ID',
            name: 'Remove',
            icon: 'mdi-delete-outline',
            action: 'mv',
            folder: 'trash',
        });
    });

});

