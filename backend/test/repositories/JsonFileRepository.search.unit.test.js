'use strict';

const fs = require('fs');
const {
    expect,
} = require('chai');
const {
    configuration,
} = require('../../src/configuration');
const {
    JsonFileRepository,
} = require('../../src/repositories/JsonFileRepository');
const {
    v4,
} = require('uuid');

const json_file_repository = JsonFileRepository.getInstance();

describe('JsonFileRepository Search', () => {

    beforeEach(() => {
        fs.rmSync(
            `${configuration.storage.files.application_files}/.application/`,
            {
                recursive: true,
                force: true,
            }
        );
    });


    it('search', async () => {
        const inserted_data = [];
        for (let i = 0; i < 5; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: 'a',
                prop_2: 'a',
                prop_3: 'a',
            });
            inserted_data.push(data);
        }

        const data = await json_file_repository.search('my_test_filename.json');

        expect(data).to.be.an('array');
        expect(data.length).to.equal(5);
    });


    it('search with filter param', async () => {
        const inserted_data = [];
        for (let i = 0; i < 20; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: 'a',
                prop_2: v4(),
                prop_3: v4(),
            });
            inserted_data.push(data);
        }

        let data = await json_file_repository.search('my_test_filename.json', {
            prop_2: inserted_data[10].prop_2,
            prop_3: inserted_data[10].prop_3,
        });
        expect(data).to.be.an('array');
        expect(data.length).to.equal(1);

        data = await json_file_repository.search('my_test_filename.json', {
            prop_1: 'b',
            prop_2: inserted_data[10].prop_2,
            prop_3: inserted_data[10].prop_3,
        });
        expect(data).to.be.an('array');
        expect(data.length).to.equal(0);


        data = await json_file_repository.search('my_test_filename.json', {
            prop_2: inserted_data[10].prop_2,
            prop_3: inserted_data[10].prop_3,
            unknown_prop: 'hello',
        });
        expect(data).to.be.an('array');
        expect(data.length).to.equal(0);
    });


    it('search with order on integer number', async () => {
        const inserted_data = [];
        for (let i = 0; i < 20; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: 'a',
                prop_2: v4(),
                prop_3: v4(),
                number_field: i,
            });
            inserted_data.push(data);
        }

        let data = await json_file_repository.search('my_test_filename.json', {}, {
            sort: {
                number_field: 1,
            },
        });
        expect(data).to.be.an('array');
        expect(data[0].number_field).to.equal(0);
        expect(data[19].number_field).to.equal(19);


        data = await json_file_repository.search('my_test_filename.json', {}, {
            sort: {
                number_field: -1,
            },
        });
        expect(data).to.be.an('array');
        expect(data[0].number_field).to.equal(19);
        expect(data[19].number_field).to.equal(0);

        try {
            await json_file_repository.search('my_test_filename.json', {}, {
                sort: {
                    number_field: -1,
                    prop_3: 1,
                },
            });
        } catch (error) {
            expect(error).to.be.an('error');
            expect(error.message).to.equal('API Limitation: only on sort field is accepted');
        }
    });


    it('search with order on float number', async () => {
        const inserted_data = [];
        for (let i = 0; i < 20; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: 'a',
                prop_2: v4(),
                prop_3: v4(),
                number_field: i / 1.1,
            });
            inserted_data.push(data);
        }

        let data = [];

        data = await json_file_repository.search('my_test_filename.json', {}, {
            sort: {
                number_field: -1,
            },
        });
        expect(data).to.be.an('array');

        for (let i = 0; i < data.length; i++) {
            if (i < data.length - 1) {
                const check = data[i].number_field > data[i + 1].number_field;
                expect(check).to.equal(true);
            }
        }

        data = await json_file_repository.search('my_test_filename.json', {}, {
            sort: {
                number_field: 1,
            },
        });
        expect(data).to.be.an('array');

        for (let i = 0; i < data.length; i++) {
            if (i < data.length - 1) {
                const check = data[i].number_field < data[i + 1].number_field;
                expect(check).to.equal(true);
            }
        }

    });

});
