'use strict';

const fs = require('fs');
const {
    expect,
} = require('chai');
const {
    configuration,
} = require('../../src/configuration');
const {
    JsonFileRepository,
} = require('../../src/repositories/JsonFileRepository');
const {
    v4,
} = require('uuid');

const json_file_repository = JsonFileRepository.getInstance();

describe('JsonFileRepository', () => {

    beforeEach(() => {
        fs.rmSync(
            `${configuration.storage.files.application_files}/.application/`,
            {
                recursive: true,
                force: true,
            }
        );
    });

    it('checkAndCreateDataFolders', async () => {
        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/`)).to.equal(false);
        await json_file_repository.checkAndInitApplicationFilePath();
        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json`)).to.equal(true);
    });

    it('init', async () => {
        await json_file_repository.init('test_repository.json');
        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/`)).to.equal(true);
        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json/test_repository.json`)).to.equal(true);
    });

    it('create', async () => {
        const data = await json_file_repository.create('my_test_filename.json', {
            prop_1: 'a',
            prop_2: 'a',
            prop_3: 'a',
        });
        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json/my_test_filename.json`))
            .to.equal(true);
        expect(data).to.have.property('id');
    });


    it('read', async () => {
        let last_inserted_id = '';
        for (let i = 0; i < 5; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: 'a',
                prop_2: 'a',
                prop_3: 'a',
            });
            last_inserted_id = data.id;
        }

        const data = await json_file_repository.read('my_test_filename.json', last_inserted_id);

        expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json/my_test_filename.json`))
            .to.equal(true);
        expect(data).to.be.an('object');
        expect(data).to.have.property('id', last_inserted_id);

    });


    describe('Update', () => {
        it('update', async () => {
            const inserted_data = [];
            for (let i = 0; i < 5; i++) {
                const data = await json_file_repository.create('my_test_filename.json', {
                    prop_1: 'a',
                    prop_2: 'a',
                    prop_3: 'a',
                });
                inserted_data.push(data);
            }

            await json_file_repository.update('my_test_filename.json', inserted_data[2].id, {
                prop_1: 'b',
            });

            const data = await json_file_repository.read('my_test_filename.json', inserted_data[2].id);

            expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json/my_test_filename.json`))
                .to.equal(true);
            expect(data).to.be.an('object');
            expect(data).to.have.property('id', inserted_data[2].id);
            expect(data).to.have.property('prop_1', 'b');

        });


        it('update with replace property', async () => {
            const inserted_data = [];
            for (let i = 0; i < 5; i++) {
                const data = await json_file_repository.create('my_test_filename.json', {
                    prop_1: 'a',
                    prop_2: 'a',
                    prop_3: 'a',
                });
                inserted_data.push(data);
            }

            await json_file_repository.update(
                'my_test_filename.json',
                inserted_data[2].id, {
                    id: inserted_data[2].id,
                    prop_1: 'b',
                },
                true
            );

            const data = await json_file_repository.read('my_test_filename.json', inserted_data[2].id);

            expect(fs.existsSync(`${configuration.storage.files.application_files}/.application/json/my_test_filename.json`))
                .to.equal(true);
            expect(data).to.be.an('object');
            expect(data).to.have.property('id', inserted_data[2].id);
            expect(data).to.have.property('prop_1', 'b');
            expect(data).to.not.have.property('prop_2');
            expect(data).to.not.have.property('prop_3');

        });


        it('update Throw No item with id', async () => {
            const inserted_data = [];
            for (let i = 0; i < 5; i++) {
                const data = await json_file_repository.create('my_test_filename.json', {
                    prop_1: 'a',
                    prop_2: 'a',
                    prop_3: 'a',
                });
                inserted_data.push(data);
            }

            try {
                await json_file_repository.update('my_test_filename.json', 'NOT_EXISTING_ID', {
                    prop_1: 'b',
                });
            } catch (error) {
                expect(error).to.be.an('error');
                expect(error.message).to.equal('No item with id: NOT_EXISTING_ID');
            }
        });
    });


    describe('Delete', () => {
        it('delete', async () => {
            const inserted_data = [];
            for (let i = 0; i < 5; i++) {
                const data = await json_file_repository.create('my_test_filename.json', {
                    prop_id: i,
                    prop_1: 'a',
                });
                inserted_data.push(data);
            }

            const id_to_delete = inserted_data[2].id;

            const before_data_length = await json_file_repository.search('my_test_filename.json');
            expect(before_data_length.length).to.equal(5);

            await json_file_repository.delete('my_test_filename.json', id_to_delete);

            const after_data_length = await json_file_repository.search('my_test_filename.json');
            expect(after_data_length.length).to.equal(4);

        });

        it('delete No item with id', async () => {
            const inserted_data = [];
            for (let i = 0; i < 5; i++) {
                const data = await json_file_repository.create('my_test_filename.json', {
                    prop_id: i,
                    prop_1: 'a',
                });
                inserted_data.push(data);
            }

            try {
                await json_file_repository.delete('my_test_filename.json', 'NOT_EXISTING_ID');
            } catch (error) {
                expect(error).to.be.an('error');
                expect(error.message).to.equal('No item with id: NOT_EXISTING_ID');
            }

        });
    });

    it('large dataset create', async () => {
        const count = 100;
        const inserted_data = [];
        for (let i = 0; i < count; i++) {
            const data = await json_file_repository.create('my_test_filename.json', {
                prop_1: v4(),
                prop_2: v4(),
                prop_3: v4(),
            });
            inserted_data.push(data);
        }

        const data = await json_file_repository.search('my_test_filename.json');

        expect(data).to.be.an('array');
        expect(data.length).to.equal(count);

    }).timeout(50000);


    it('JsonFileRepository formated_json_file: false,', async () => {
        const filename = 'my_test_filename_second.json';

        const _json_file_repository = new JsonFileRepository();
        _json_file_repository.options = {
            formated_json_file: false,
        };
        const count = 10;
        const inserted_data = [];
        for (let i = 0; i < count; i++) {
            const data = await _json_file_repository.create(
                filename,
                {
                    prop_1: v4(),
                    prop_2: v4(),
                    prop_3: v4(),
                }
            );
            inserted_data.push(data);
        }

        const data = await _json_file_repository.search(filename);

        expect(data).to.be.an('array');
        expect(data.length).to.equal(count);

    });


    it('JsonFileRepository createMany autoid = false', async () => {
        const _json_file_repository = new JsonFileRepository();
        const filename = 'my_test_filename_create_many.json';
        const create_many_seed = [
            'aaaa',
            'bbbb',
            'cccc',
            'dddd',
        ];

        await _json_file_repository.createMany(filename, create_many_seed, false);
        const data = await _json_file_repository.search(filename);

        expect(data).to.deep.equal(create_many_seed);

    });

});
