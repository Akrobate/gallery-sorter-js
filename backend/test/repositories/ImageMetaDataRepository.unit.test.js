
const {
    expect,
} = require('chai');

const {
    ImageMetaDataRepository,
} = require('../../src/repositories');
const {
    removeTestApplicationFolder,
    createGalleriesJsonFile,
} = require('../test_helpers/test_helpers');

describe('ImageMetaDataRepository unit', () => {

    beforeEach(async () => {
        await removeTestApplicationFolder();
        await createGalleriesJsonFile();
    });

    it('search, create, update', async () => {
        const image_meta_data_repository = ImageMetaDataRepository.getInstance();
        const results = await image_meta_data_repository.search();
        expect(results).to.be.an('array').and.to.have.length(0);

        const result = await image_meta_data_repository.create({
            prompt: 'Test, Other',
            gallery_id_list: [{}],
        });
        expect(result).to.be.an('object');
        const results_1 = await image_meta_data_repository.search();
        expect(results_1).to.be.an('array').and.to.have.length(1);

        const result_2 = await image_meta_data_repository.update(
            results_1[0].id,
            {
                prompt: 'Test, Other UPDATED',
                gallery_id_list: [{}],
            }
        );
        expect(result_2).to.be.an('object');
        const results_3 = await image_meta_data_repository.search();
        expect(results_3).to.be.an('array').and.to.have.length(1);
        expect(results_3[0]).to.have.property('prompt', 'Test, Other UPDATED');
    });

});

