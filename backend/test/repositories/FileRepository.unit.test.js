const fs = require('fs');

const {
    expect,
} = require('chai');

const {
    FileRepository,
} = require('../../src/repositories');


describe('FileRepository unit', () => {

    const test_file = 'test/data/test_random_file.txt';

    beforeEach(() => {
        fs.writeFileSync(test_file, 'data');
    });


    it('exists', async () => {
        const prompt_repository = FileRepository.getInstance();
        const result = await prompt_repository.exists(test_file);
        expect(result).to.equal(true);
    });

    it('moveFile', async () => {
        const prompt_repository = FileRepository.getInstance();
        await prompt_repository.moveFile(test_file, 'test/data/test_random_file_2.txt');
        let exists = await prompt_repository.exists('test/data/test_random_file_2.txt');
        expect(exists).to.equal(true);
        await prompt_repository.moveFile('test/data/test_random_file_2.txt', test_file);
        exists = await prompt_repository.exists(test_file);
        expect(exists).to.equal(true);

    });

    it('appendFile', async () => {
        const prompt_repository = FileRepository.getInstance();
        const result = await prompt_repository.appendFile(test_file, 'data');
        expect(result).to.equal(undefined);
    });

});

