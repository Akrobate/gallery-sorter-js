
const {
    expect,
} = require('chai');

const {
    GalleryRepository,
} = require('../../src/repositories');


describe('GalleryRepository unit', () => {

    const gallery_repository = GalleryRepository.getInstance();

    it('formatGalleryItem display not setted', () => {

        const result = gallery_repository.formatGalleryItem({
            id: 1,
            name: 'Test',
            tag_list: ['Test'],
            archived: false,
        });

        expect(result).to.deep.equal({
            id: 1,
            name: 'Test',
            tag_list: [
                'Test',
            ],
            archived: false,
            folder: '',
            tags: [],
            actions: [],
            display: true,
        });
    });

});

